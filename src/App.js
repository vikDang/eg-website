import './App.css';
import './assets/style.scss';
import { Routes, Route } from 'react-router-dom';
import routes from './pages/index';
import { reveal } from './utils/reveal';
import { useEffect } from 'react';
import Layout from './layout/Layout';

function App() {
	useEffect(() => {
		window.addEventListener('scroll', reveal);

		return () => {
			window.removeEventListener('scroll', reveal);
		};
	}, []);
	return (
		<Layout>
			<Routes>
				{routes.map((data, index) => (
					<Route
						onUpdate={() => window.scrollTo(0, 0)}
						exact={true}
						path={data.path}
						element={data.component}
						key={index}
					/>
				))}
			</Routes>
		</Layout>
	);
}

export default App;
