import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import { BrowserRouter } from 'react-router-dom';
import ScrollToTop from './ScrollToTop';
import store from './store';
import { Provider as ReduxProvider } from 'react-redux';

ReactDOM.render(
	<BrowserRouter>
		<ScrollToTop />
		<ReduxProvider store={store}>
			<App />
		</ReduxProvider>
	</BrowserRouter>,
	document.getElementById('root')
);
