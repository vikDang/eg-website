import { createSlice } from '@reduxjs/toolkit';

const initialState = {
	isLoading: false,
	error: false,
	footer: {
		description: null,
		buttonContent: null,
		followUs: null,
		joinUsOn: null,
		id: null,
	},
};

const slice = createSlice({
	name: 'footer',
	initialState,
	reducers: {
		getFooter: (state, action) => {
			const footer = action.payload;
			state.footer = footer;
		},
	},
});

export const getFooter = footer => async dispatch => {
	dispatch(slice.actions.getFooter(footer));
	return footer;
};

export const { reducer, actions } = slice;
