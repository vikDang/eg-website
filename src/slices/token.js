import { createSlice } from '@reduxjs/toolkit';
import adminApi from 'api/adminAPI';

const initialState = {
	isLoading: false,
	error: false,
	token: {
		title: null,
		description: null,
		tokenImage: null,
		tokenImageMobile: null,
		id: null,
	},
	imageBase64: null,
	imageMobileBase64: null,
};

const slice = createSlice({
	name: 'token',
	initialState,
	reducers: {
		getToken: (state, action) => {
			const token = action.payload;
			state.token = token;
		},
		updateTokenDescription: (state, action) => {
			const description = action.payload;
			state.token.description = description;
		},
		updateTokenImage: (state, action) => {
			const image = action.payload;
			state.token.tokenImage = image;
		},
		updateImageBase64: (state, action) => {
			const base64Image = action.payload;
			state.imageBase64 = base64Image;
		},
		updateTokenImageMobile: (state, action) => {
			const image = action.payload;
			state.token.tokenImageMobile = image;
		},
		updateImageMobileBase64: (state, action) => {
			const base64Image = action.payload;
			state.imageMobileBase64 = base64Image;
		},
	},
});

export const getToken = token => async dispatch => {
	dispatch(slice.actions.getToken(token));
	return token;
};

export const updateTokenDescription = request => async dispatch => {
	const response = await adminApi.updateTokenDescription(request);
	dispatch(slice.actions.getToken(response));
	return response;
};

export const updateTokenImage = request => async dispatch => {
	const response = await adminApi.updateTokenImage(request);
	dispatch(slice.actions.getToken(response));
	return response;
};

export const updateTokenImageMobile = request => async dispatch => {
	const response = await adminApi.updateTokenImageMobile(request);
	dispatch(slice.actions.getToken(response));
	return response;
};

export const { reducer, actions } = slice;
