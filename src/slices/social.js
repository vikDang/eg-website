import { createSlice } from '@reduxjs/toolkit';
import adminApi from 'api/adminAPI';
import objFromArray from 'utils/objFromArray';

const initialState = {
	isLoading: false,
	error: false,
	socials: {
		byId: {},
		allIds: [],
	},
};

const slice = createSlice({
	name: 'social',

	initialState,
	reducers: {
		getSocials: (state, action) => {
			const socials = action.payload;
			state.socials.byId = objFromArray(socials);
			state.socials.allIds = Object.keys(state.socials.byId);
		},
		createSocial: (state, action) => {
			const socialItem = action.payload;
			state.socials.byId[socialItem.id] = socialItem;
			state.socials.allIds.push(socialItem.id);
		},
		updateSocial: (state, action) => {
			const socialItem = action.payload;
			state.socials.byId[socialItem.id] = socialItem;
		},
		deleteSocial: (state, action) => {
			const socialId = action.payload;

			delete state.socials.byId[socialId];
			state.socials.allIds.filter(_socialId => _socialId !== socialId);
		},
	},
});

export const createSocial = socialItem => async dispatch => {
	dispatch(slice.actions.createSocial(socialItem));
};

export const updateSocial = update => async dispatch => {
	dispatch(slice.actions.updateSocial(update));
};

export const updateSocials = listSocial => async dispatch => {
	const response = await adminApi.updateSocials({ data: listSocial });
	dispatch(slice.actions.getSocials(response));
	return response;
};

export const deleteSocial = socialId => async dispatch => {
	dispatch(slice.actions.deleteSocial(socialId));
};

export const { reducer, actions } = slice;
