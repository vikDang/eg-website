import { createSlice } from '@reduxjs/toolkit';
import adminApi from 'api/adminAPI';
import objFromArray from 'utils/objFromArray';

const initialState = {
	isLoading: false,
	error: false,
	menus: {
		byId: {},
		allIds: [],
	},
};

const slice = createSlice({
	name: 'header',
	initialState,
	reducers: {
		getMenus: (state, action) => {
			const menus = action.payload;
			state.menus.byId = objFromArray(menus);
			state.menus.allIds = Object.keys(state.menus.byId);
		},
		createMenu: (state, action) => {
			const menuItem = action.payload;
			state.menus.byId[menuItem.id] = menuItem;
			state.menus.allIds.push(menuItem.id);
		},
		updateMenu: (state, action) => {
			const menuItem = action.payload;
			state.menus.byId[menuItem.id] = menuItem;
		},
		deleteMenu: (state, action) => {
			const menuId = action.payload;

			delete state.menus.byId[menuId];
			state.menus.allIds.filter(_menuId => _menuId !== menuId);
		},
	},
});

export const createMenu = menuItem => async dispatch => {
	dispatch(slice.actions.createMenu(menuItem));
};

export const updateMenu = update => async dispatch => {
	dispatch(slice.actions.updateMenu(update));
};

export const updateMenus = listMenu => async dispatch => {
	const response = await adminApi.updateMenus({ listMenu });
	dispatch(slice.actions.getMenus(response));
	return response;
};

export const deleteMenu = menuId => async dispatch => {
	dispatch(slice.actions.deleteMenu(menuId));
};

export const { reducer, actions } = slice;
