import { createSlice } from '@reduxjs/toolkit';
import adminApi from 'api/adminAPI';

const initialState = {
	isLoading: false,
	error: false,
	trailer: { trailer: null },
};

const slice = createSlice({
	name: 'trailer',
	initialState,
	reducers: {
		getTrailer: (state, action) => {
			const trailer = action.payload;
			state.trailer = trailer;
		},
		updateTrailerLink: (state, action) => {
			const link = action.payload;
			state.trailer.trailer = link;
		},
	},
});

export const getTrailer = trailer => async dispatch => {
	dispatch(slice.actions.getTrailer(trailer));
	return trailer;
};

export const updateTrailerLink = request => async dispatch => {
	const response = await adminApi.updateTrailer(request);
	dispatch(slice.actions.updateTrailerLink(response));
	return response;
};

export const { reducer, actions } = slice;
