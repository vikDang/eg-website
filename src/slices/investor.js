import { createSlice } from '@reduxjs/toolkit';
import adminApi from 'api/adminAPI';
import objFromArray from 'utils/objFromArray';

const initialState = {
	isLoading: false,
	error: false,
	title: null,
	comingSoonText: null,
	investors: {
		byId: {},
		allIds: [],
	},
};

const slice = createSlice({
	name: 'investor',
	initialState,
	reducers: {
		getInvestors: (state, action) => {
			const investors = action.payload;
			const investorList = investors.map(investor => ({ image: investor }));
			state.investors.byId = objFromArray(investorList, 'image');
			state.investors.allIds = Object.keys(state.investors.byId);
		},
		updateInvestorContent: (state, action) => {
			const { title, comingSoonText } = action.payload;
			state.title = title;
			state.comingSoonText = comingSoonText;
		},
		createInvestor: (state, action) => {
			const investorImage = action.payload;
			const newInvestor = { image: investorImage };
			state.investors.byId[newInvestor.image] = newInvestor;
			state.investors.allIds.push(newInvestor.image);
		},
		updateInvestor: (state, action) => {
			const investorItem = action.payload;
			state.investors.byId[investorItem.image] = investorItem;
		},

		deleteInvestor: (state, action) => {
			const investorId = action.payload;

			delete state.investors.byId[investorId];
			state.investors.allIds.filter(_investorId => _investorId !== investorId);
		},
	},
});

export const createInvestor = request => async dispatch => {
	const response = await adminApi.createInvestors(request);
	dispatch(slice.actions.getInvestors(response.list));
};

export const updateInvestors = listInvestor => async dispatch => {
	const response = await adminApi.updateInvestorPosition({ listFileName: listInvestor });
	dispatch(slice.actions.getInvestors(response.list));
	return response;
};

export const updateInvestor = update => async dispatch => {
	dispatch(slice.actions.updateInvestor(update));
};

export const deleteInvestor = investorImage => async dispatch => {
	await adminApi.deleteInvestor(investorImage);
	dispatch(slice.actions.deleteInvestor(investorImage));
};

export const { reducer, actions } = slice;
