import { createSlice } from '@reduxjs/toolkit';
import adminApi from 'api/adminAPI';

const initialState = {
	isLoading: false,
	error: false,
	banners: {
		topBanner: {
			backgroundImage: null,
			title: null,
			buttons: [],
		},
		bottomBanner: { backgroundImage: null, title: null },
	},
};

const slice = createSlice({
	name: 'banner',
	initialState,
	reducers: {
		getTopBanner: (state, action) => {
			const banner = action.payload;
			state.banners.topBanner = banner;
		},
		getTopImageBase64: (state, action) => {
			const imageBase64 = action.payload;
			state.banners.topBanner.imageBase64 = imageBase64;
		},
		updateTopBannerContent: (state, action) => {
			const { title, buttons } = action.payload;
			state.banners.topBanner = { ...state.banners.topBanner, title, buttons };
		},

		getBottomBanner: (state, action) => {
			const banner = action.payload;
			state.banners.bottomBanner = banner;
		},

		getBottomBannerBackGround: (state, action) => {
			const backgroundImage = action.payload;
			state.banners.bottomBanner.backgroundImage = backgroundImage;
		},
		getBottomImageBase64: (state, action) => {
			const imageBase64 = action.payload;
			state.banners.bottomBanner.imageBase64 = imageBase64;
		},
	},
});

export const getTopBanner = banner => async dispatch => {
	dispatch(slice.actions.getTopBanner(banner));
	return banner;
};
export const getBottomBanner = () => async dispatch => {
	const banner = {
		backgroundImage: 'https://luv.vn/wp-content/uploads/2021/08/hinh-anh-gai-xinh-9.jpg',
		title: 'Bottom Image',
	};
	dispatch(slice.actions.getBottomBanner(banner));
	return banner;
};

export const updateTopBannerContent = request => async dispatch => {
	const response = await adminApi.updateTopBannerContent(request);

	dispatch(slice.actions.getTopBanner(response));
	return response;
};

export const updateTopBannerImage = request => async dispatch => {
	const response = await adminApi.updateTopBannerImage(request);

	dispatch(slice.actions.getTopBanner(response));

	return response;
};

export const updateBottomBannerImage = request => async dispatch => {
	const response = await adminApi.updateBottomBannerImage(request);

	dispatch(slice.actions.getBottomBannerBackGround(response));

	return response;
};

export const { reducer, actions } = slice;
