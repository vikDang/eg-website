import { createSlice } from '@reduxjs/toolkit';
import adminApi from 'api/adminAPI';

const initialState = {
	isLoading: false,
	error: false,
	roadmap: {
		title: null,
		info: [],
		contentImage: null,
		topImage: null,
		bottomImage: null,
		id: null,
	},
	contentImageBase64: null,
	topImageBase64: null,
	bottomImageBase64: null,
};

const slice = createSlice({
	name: 'roadmap',
	initialState,
	reducers: {
		getRoadmap: (state, action) => {
			const roadmap = action.payload;
			state.roadmap = roadmap;
		},
		updateRoadmapInfo: (state, action) => {
			const infos = action.payload;
			state.roadmap.info = infos;
		},
		updateRoadmapContentImage: (state, action) => {
			const image = action.payload;
			state.roadmap.contentImage = image;
		},
		updateContentImageBase64: (state, action) => {
			const base64Image = action.payload;
			state.contentImageBase64 = base64Image;
		},
		updateRoadmapImage: (state, action) => {
			const image = action.payload;
			state.roadmap.topImage = image;
		},
		updateTopImageBase64: (state, action) => {
			const base64Image = action.payload;
			state.topImageBase64 = base64Image;
		},
		updateBottomImage: (state, action) => {
			const image = action.payload;
			state.roadmap.bottomImage = image;
		},
		updateBottomImageBase64: (state, action) => {
			const base64Image = action.payload;
			state.bottomImageBase64 = base64Image;
		},
	},
});

export const getRoadmap = roadmap => async dispatch => {
	dispatch(slice.actions.getRoadmap(roadmap));
	return roadmap;
};

export const updateRoadmapInfo = request => async dispatch => {
	const response = await adminApi.updateRoadmapInfo(request);
	dispatch(slice.actions.getRoadmap(response.roadmap));
	return response;
};

export const updateRoadmapTopImage = request => async dispatch => {
	const response = await adminApi.updateRoadmapTopImage(request);
	// Clear current display image
	dispatch(slice.actions.updateTopImageBase64(null));

	// Update roadmap state
	dispatch(slice.actions.getRoadmap(response));
	return response;
};

export const updateRoadmapBottomImage = request => async dispatch => {
	const response = await adminApi.updateRoadmapBottomImage(request);
	// Clear current display image
	dispatch(slice.actions.updateBottomImageBase64(null));

	// Update roadmap state
	dispatch(slice.actions.getRoadmap(response));
	return response;
};

export const { reducer, actions } = slice;
