import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import { configSettings } from 'utils/config';

const initialState = {
	language: 'en',
};

export const restoreSettings = createAsyncThunk('settings/init', () => {
	let settings = null;

	const storageSettings = configSettings.get();

	if (storageSettings) {
		settings = storageSettings;
	} else {
		settings = {};
	}

	return settings ? settings : initialState;
});

const slice = createSlice({
	name: 'settings',
	initialState,
	reducers: {
		save: (state, action) => {
			state = Object.assign(state, action.payload);
		},
	},
	extraReducers: builder => {
		builder.addCase(restoreSettings.fulfilled, (state, action) => {
			state = Object.assign(state, action.payload);
		});
	},
});

export const selectorSettings = state => state.settings;
export const { reducer } = slice;
export default slice;
