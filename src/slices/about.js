import { createSlice } from '@reduxjs/toolkit';
import adminApi from 'api/adminAPI';
import objFromArray from 'utils/objFromArray';

const initialState = {
	isLoading: false,
	error: false,
	abouts: {
		byId: {},
		allIds: [],
	},
};

const slice = createSlice({
	name: 'about',
	initialState,
	reducers: {
		getAbouts: (state, action) => {
			const abouts = action.payload;
			state.abouts.byId = objFromArray(abouts);
			state.abouts.allIds = Object.keys(state.abouts.byId);
		},
		createAbout: (state, action) => {
			const about = action.payload;
			state.abouts.byId[about.id] = about;
			state.abouts.allIds.push(about.id);
		},
		updateAbout: (state, action) => {
			const about = action.payload;
			state.abouts.byId[about.id] = about;
		},
		deleteAbout: (state, action) => {
			const aboutId = action.payload;

			delete state.abouts.byId[aboutId];
			state.abouts.allIds.filter(_aboutId => _aboutId !== aboutId);
		},
	},
});

export const createAbout = request => async dispatch => {
	const response = await adminApi.createAbout(request);
	dispatch(slice.actions.createAbout(response));
};

export const updateAboutContent = (request, id) => async dispatch => {
	const response = await adminApi.updateAboutContent(request, id);
	dispatch(slice.actions.updateAbout(response));
};

export const updateAboutImage = (request, id) => async dispatch => {
	const response = await adminApi.updateAboutImage(request, id);
	dispatch(slice.actions.updateAbout(response));
};

export const updateAboutIcon = (request, id) => async dispatch => {
	const response = await adminApi.updateAboutIcon(request, id);
	dispatch(slice.actions.updateAbout(response));
};

export const deleteAbout = aboutId => async dispatch => {
	await adminApi.deleteAbout(aboutId);
	dispatch(slice.actions.deleteAbout(aboutId));
};

export const { reducer, actions } = slice;
