import { createSlice } from '@reduxjs/toolkit';
import adminApi from 'api/adminAPI';
import objFromArray from 'utils/objFromArray';

const initialState = {
	isLoading: false,
	error: false,
	title: null,
	characters: {
		byId: {},
		allIds: [],
	},
};

const slice = createSlice({
	name: 'character',
	initialState,
	reducers: {
		getCharacters: (state, action) => {
			const characters = action.payload;
			state.characters.byId = objFromArray(characters);
			state.characters.allIds = Object.keys(state.characters.byId);
		},
		updateCharacterTitle: (state, action) => {
			const title = action.payload;
			state.title = title;
		},
		createCharacter: (state, action) => {
			const character = action.payload;
			state.characters.byId[character.id] = character;
			state.characters.allIds.push(character.id);
		},
		updateCharacter: (state, action) => {
			const character = action.payload;
			state.characters.byId[character.id] = character;
		},
		updateAvatarBase64: (state, action) => {
			const { character, avatarBase64 } = action.payload;
			state.characters.byId[character.id].avatarBase64 = avatarBase64;
		},
		updateIconBase64: (state, action) => {
			const { character, iconBase64 } = action.payload;
			state.characters.byId[character.id].iconBase64 = iconBase64;
		},
		updateIconInactiveBase64: (state, action) => {
			const { character, iconInactiveBase64 } = action.payload;
			state.characters.byId[character.id].iconInactiveBase64 = iconInactiveBase64;
		},

		deleteCharacter: (state, action) => {
			const characterId = action.payload;

			delete state.characters.byId[characterId];
			state.characters.allIds.filter(_characterId => _characterId !== characterId);
		},
		createSkill: (state, action) => {
			const { skill, characterId } = action.payload;
			const character = state.characters.byId[characterId];
			character.skills.push(skill);
			state.characters.byId[characterId] = character;
		},
		updateSkill: (state, action) => {
			const { skill, characterId } = action.payload;
			const character = state.characters.byId[characterId];

			const skillIndex = character.skills.findIndex(x => x.id === skill.id);
			state.characters.byId[characterId].skills[skillIndex] = skill;
		},
		deleteSkill: (state, action) => {
			const { skillId, characterId } = action.payload;
			const character = state.characters.byId[characterId];

			const skillIndex = character.skills.findIndex(x => x.id === skillId);
			state.characters.byId[characterId].skills.splice(skillIndex, 1);
		},
	},
});

export const getCharacters = characters => async dispatch => {
	dispatch(slice.actions.getCharacters(characters));
};

export const createCharacter = request => async dispatch => {
	const response = await adminApi.createCharacter(request);
	dispatch(slice.actions.createCharacter(response));
};

export const updateCharacterContent = (request, characterId) => async dispatch => {
	const response = await adminApi.updateCharacterContent(characterId, request);
	dispatch(slice.actions.updateCharacter(response));
};

export const updateCharacterAvatar = (request, characterId) => async dispatch => {
	const response = await adminApi.updateCharacterAvatar(characterId, request);
	dispatch(slice.actions.updateCharacter(response));
};

export const updateCharacterIcon = (request, characterId) => async dispatch => {
	const response = await adminApi.updateCharacterIcon(characterId, request);
	dispatch(slice.actions.updateCharacter(response));
};

export const updateCharacterIconInactive = (request, characterId) => async dispatch => {
	const response = await adminApi.updateCharacterIconActive(characterId, request);
	dispatch(slice.actions.updateCharacter(response));
};

export const createSkill = (characterId, request) => async dispatch => {
	const response = await adminApi.createSkill(characterId, request);
	const newSkill = response.skills.pop();

	dispatch(slice.actions.createSkill({ skill: newSkill, characterId }));
};

export const updateSkillInfo = (request, characterId, skillId) => async dispatch => {
	const response = await adminApi.updateSkillInfo(characterId, skillId, request);
	const skillUpdated = response.skills?.find(skill => skill.id === skillId);
	dispatch(slice.actions.updateSkill({ skill: skillUpdated, characterId }));
};

export const updateSkillIcon = (request, characterId, skillId) => async dispatch => {
	const response = await adminApi.updateSkillIcon(characterId, skillId, request);
	const skillUpdated = response.skills?.find(skill => skill.id === skillId);
	dispatch(slice.actions.updateSkill({ skill: skillUpdated, characterId }));
};

export const deleteCharacter = characterId => async dispatch => {
	await adminApi.deleteCharacter(characterId);
	dispatch(slice.actions.deleteCharacter(characterId));
};

export const deleteSkill = (characterId, skillId) => async dispatch => {
	await adminApi.deleteSkill(characterId, skillId);
	dispatch(slice.actions.deleteSkill({ characterId, skillId }));
};

export const { reducer, actions } = slice;
