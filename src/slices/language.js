import { createSlice } from '@reduxjs/toolkit';
import adminApi from 'api/adminAPI';
import objFromArray from 'utils/objFromArray';

const initialState = {
	isLoading: false,
	error: false,
	languages: {
		byId: {},
		allIds: [],
	},
};

const slice = createSlice({
	name: 'language',
	initialState,
	reducers: {
		getLanguages: (state, action) => {
			const languages = action.payload;

			state.languages.byId = objFromArray(languages);
			state.languages.allIds = Object.keys(state.languages.byId);
		},
		createLanguage: (state, action) => {
			const language = action.payload;
			state.languages.byId[language.id] = language;
			state.languages.allIds.push(language.id);
		},
		updateLanguage: (state, action) => {
			const language = action.payload;
			state.languages.byId[language.id] = language;
		},

		deleteLanguage: (state, action) => {
			const languageId = action.payload;

			delete state.languages.byId[languageId];
			state.languages.allIds.filter(_languageId => _languageId !== languageId);
		},
	},
});

export const createLanguage = language => async dispatch => {
	dispatch(slice.actions.createLanguage(language));
};

export const updateLanguages = languages => async dispatch => {
	const response = await adminApi.updateLanguages({ languages });
	dispatch(slice.actions.getLanguages(response.languages));
	return response;
};

export const updateLanguage = update => async dispatch => {
	dispatch(slice.actions.updateLanguage(update));
};

export const deleteLanguage = languageImage => async dispatch => {
	dispatch(slice.actions.deleteLanguage(languageImage));
};

export const { reducer, actions } = slice;
