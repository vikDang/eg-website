export function reveal() {
	const reveals = document.querySelectorAll('.reveal');

	for (let i = 0; i < reveals.length; i++) {
		const rect = reveals[i].getBoundingClientRect();

		if (rect.bottom > 0 && rect.y < window.innerHeight) {
			reveals[i].classList.add('active');
		} else {
			reveals[i].classList.remove('active');
		}
	}
}
