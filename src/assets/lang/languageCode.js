export const languageCodeFull = [
	{ code: 'en', name: 'English', nativeName: 'English', icon: '/static/lang/english.png' },
	{ code: 'ko', name: 'Korean', nativeName: 'Korean', icon: '/static/lang/korean.png' },
	{ code: 'ja', name: 'Japan', nativeName: '日本語 (にほんご／にっぽんご)', icon: '/static/lang/japanese.png' },
];
