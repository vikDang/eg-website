import imgGame from '../images/games/banner.png';

const heroSliderData = [
	{
		title_1: 'Eternal Glory',
		title_2: 'Sell extraordinary',
		title_3: 'Monster NFTs',
		description: 'Embark on the revoluntionary journey, fight for the land of Eternal and for its glory',
		img: imgGame,
		imgbg: imgGame,
		class: 'center',
	},
];

export default heroSliderData;
