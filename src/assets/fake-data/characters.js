import swordman from '../images/characters/swordman/swordman.png';
import rogue from '../images/characters/rogue/rogue.png';
import axeman from '../images/characters/axeman/axeman.png';
import wizard from '../images/characters/wizard/wizard.png';
import bowman from '../images/characters/bowman/bowman.png';
import gunner from '../images/characters/gunner/gunner.png';

import swordmanIcon from '../images/characters/swordman/swordman-icon.png';
import rogueIcon from '../images/characters/rogue/rogue-icon.png';
import axemanIcon from '../images/characters/axeman/axeman-icon.png';
import wizardIcon from '../images/characters/wizard/wizard-icon.png';
import bowmanIcon from '../images/characters/bowman/bowman-icon.png';
import gunnerIcon from '../images/characters/gunner/gunner-icon.png';

import swordmanIconActive from '../images/characters/swordman/swordman-active-icon.png';
import rogueIconActive from '../images/characters/rogue/rogue-active-icon.png';
import axemanIconActive from '../images/characters/axeman/axeman-active-icon.png';
import wizardIconActive from '../images/characters/wizard/wizard-active-icon.png';
import bowmanIconActive from '../images/characters/bowman/bowman-active-icon.png';
import gunnerIconActive from '../images/characters/gunner/gunner-active-icon.png';

import swordmanSkill1 from '../images/characters/swordman/skills/1.png';
import swordmanSkill2 from '../images/characters/swordman/skills/2.png';
import swordmanSkill3 from '../images/characters/swordman/skills/3.png';

import rogueSkill1 from '../images/characters/rogue/skills/1.png';
import rogueSkill2 from '../images/characters/rogue/skills/2.png';
import rogueSkill3 from '../images/characters/rogue/skills/3.png';

import axemanSkill1 from '../images/characters/axeman/skills/1.png';
import axemanSkill2 from '../images/characters/axeman/skills/2.png';
import axemanSkill3 from '../images/characters/axeman/skills/3.png';

import wizardSkill1 from '../images/characters/wizard/skills/1.png';
import wizardSkill2 from '../images/characters/wizard/skills/2.png';
import wizardSkill3 from '../images/characters/wizard/skills/3.png';

import bowmanSkill1 from '../images/characters/bowman/skills/1.png';
import bowmanSkill2 from '../images/characters/bowman/skills/2.png';
import bowmanSkill3 from '../images/characters/bowman/skills/3.png';

import gunnerSkill1 from '../images/characters/gunner/skills/1.png';
import gunnerSkill2 from '../images/characters/gunner/skills/2.png';
import gunnerSkill3 from '../images/characters/gunner/skills/3.png';

const characters = [
	{
		name: 'Rogue',
		icon: rogueIcon,
		iconActive: rogueIconActive,
		title: '',
		img: rogue,
		description: 'Rogues are ruthless and stealthy bounty hunters. They are the deadliest force of the Eternal Land.',
		skills: [
			{
				name: 'Stun Attack',
				icon: rogueSkill1,
			},
			{
				name: 'Skull Break',
				icon: rogueSkill2,
			},
			{
				name: 'Chest Break',
				icon: rogueSkill3,
			},
		],
	},
	{
		name: 'Axeman',
		icon: axemanIcon,
		iconActive: axemanIconActive,
		title: '',
		img: axeman,
		description: 'Axemen wield huge, heavy weapons and shields. They act as strong frontlines in battles',
		skills: [
			{
				name: 'Power Recharge',
				icon: axemanSkill1,
			},
			{
				name: 'Fury Shield',
				icon: axemanSkill2,
			},
			{
				name: 'Far Move',
				icon: axemanSkill3,
			},
		],
	},
	{
		name: 'Swordman',
		icon: swordmanIcon,
		iconActive: swordmanIconActive,
		title: '',
		img: swordman,
		description: 'Swordmen are fearless, strong-willed fighters. They are masters of swordsmanship ',
		skills: [
			{
				name: 'Ultimate Transform',
				icon: swordmanSkill1,
			},
			{
				name: 'Shield Mastery',
				icon: swordmanSkill2,
			},
			{
				name: 'Shadow Evade',
				icon: swordmanSkill3,
			},
		],
	},
	{
		name: 'Wizard',
		icon: wizardIcon,
		iconActive: wizardIconActive,
		title: '',
		img: wizard,
		description:
			'Wizards use magic derived from supernatural, occult, or arcane sources. They are long-range heroes with high movement speed and low damage. ',
		skills: [
			{
				name: 'Vampire Summon',
				icon: wizardSkill1,
			},
			{
				name: 'Vampire Attack',
				icon: wizardSkill2,
			},
			{
				name: 'Stun Spell',
				icon: wizardSkill3,
			},
		],
	},
	{
		name: 'Bowman',
		icon: bowmanIcon,
		iconActive: bowmanIconActive,
		title: '',
		img: bowman,
		description:
			'Bowman are masters of ranged combats. They have the ability to accurately hit their targets from long distance',
		skills: [
			{
				name: 'Health Restore',
				icon: bowmanSkill1,
			},
			{
				name: 'Enemy Decoy',
				icon: bowmanSkill2,
			},
			{
				name: 'Beast Summon',
				icon: bowmanSkill3,
			},
		],
	},
	{
		name: 'Gunner',
		icon: gunnerIcon,
		iconActive: gunnerIconActive,
		title: '',
		img: gunner,
		description: 'Gunners specialize in using guns. They are highly effective in mid-ranged combats',
		skills: [
			{
				name: 'Stun Attack',
				icon: gunnerSkill1,
			},
			{
				name: 'Strike Through',
				icon: gunnerSkill2,
			},
			{
				name: 'Plus Attack',
				icon: gunnerSkill3,
			},
		],
	},
];

export default characters;
