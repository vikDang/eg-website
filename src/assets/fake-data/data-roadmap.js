const roadmapData = [
	{ time: 'Q1 2022', listPlan: ['Market Research', 'Game Idea', 'Art Concept'] },
	{ time: 'Q2 2022', listPlan: ['White Paper', 'Website', 'Community', 'Private Sale'] },
	{ time: 'Q3 2022', listPlan: ['IDO', 'Testnet', 'Mainnet Launch', 'Genesis Hero Sale', 'Multi-chain'] },
	{ time: 'Q4 2022', listPlan: ['Gameplay 2-3', 'Weapon Upgrading', 'Breeding System', 'Advanced Job', 'Marketplace'] },
	{
		time: 'First Half 2023',
		listPlan: ['Gameplay 4+5', 'Adventure Mode', 'Cross Chain Bridge', 'Gem System', 'Gamefi platform'],
	},
	{ time: 'Second Half 2023', listPlan: ['Gameplay 6', 'Clash System', 'Layer 2 Protocol', 'Tournament'] },
];

export default roadmapData;
