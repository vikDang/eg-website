import PropTypes from 'prop-types';
import React from 'react';
import { A11y, Navigation, Scrollbar } from 'swiper';
import { Swiper, SwiperSlide } from 'swiper/react';
import YoutubeBackground from 'react-youtube-background';
import 'swiper/scss';
import 'swiper/scss/navigation';
import 'swiper/modules/pagination/pagination.scss';
import imgGame from '../../assets/images/games/banner.png';
import { Link } from 'react-router-dom';

const Slider = props => {
	const data = props.data;
	return (
		<div className="mainslider">
			<Swiper
				modules={[Navigation, Scrollbar, A11y]}
				spaceBetween={0}
				slidesPerView={1}
				navigation
				scrollbar={{ draggable: true }}
			>
				{data.map((item, index) => (
					<SwiperSlide key={index} className={item.class}>
						<SliderItem item={item} />
					</SwiperSlide>
				))}
			</Swiper>
		</div>
	);
};

Slider.propTypes = {
	data: PropTypes.array.isRequired,
	control: PropTypes.bool,
	auto: PropTypes.bool,
	timeOut: PropTypes.number,
};
const SliderItem = props => (
	<div className="video-banner">
		<YoutubeBackground videoId="5X8v3VwlMxc" style={{ height: '100%' }}>
			<div className="swiper-container mainslider home">
				<div className="swiper-wrapper">
					<div className="swiper-slide">
						<div className="slider-item">
							<div className="themesflat-container ">
								<div className="wrap-heading flat-slider flex">
									<div className="content">
										<p className="sub-heading">{props.item.description}</p>
										<div className="flat-bt-slider flex style2">
											<Link to="/explore-01" className="sc-button btn-eg">
												Whitepaper
											</Link>
											<Link to="/create-item" className="sc-button btn-eg">
												Pitch Deck
											</Link>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</YoutubeBackground>
	</div>
);
export default Slider;
