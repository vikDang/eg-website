import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { Navigation, Scrollbar, A11y } from 'swiper';
import { Swiper, SwiperSlide } from 'swiper/react';
import 'swiper/scss';
import 'swiper/modules/navigation/navigation.scss';
import 'swiper/modules/pagination/pagination.scss';

import imgGame from '../../assets/images/games/2022-03-10 09.15 3.png';

const SliderStyle1 = props => {
	const data = props.data;
	return (
		<div className="mainslider">
			<Swiper
				modules={[Navigation, Scrollbar, A11y]}
				spaceBetween={0}
				slidesPerView={1}
				navigation
				scrollbar={{ draggable: true }}
			>
				{data.map((item, index) => (
					<SwiperSlide key={index} className={item.class}>
						<SliderItem item={item} />
					</SwiperSlide>
				))}
			</Swiper>
		</div>
	);
};

SliderStyle1.propTypes = {
	data: PropTypes.array.isRequired,
};
const SliderItem = props => (
	<div className="flat-title-page" style={{ backgroundImage: `url(${imgGame})` }}>
		<div className="swiper-container mainslider home">
			<div className="swiper-wrapper">
				<div className="swiper-slide">
					<div className="slider-item">
						<div className="themesflat-container ">
							<div className="wrap-heading flat-slider flex">
								<div className="content">
									<h2 className="heading">{props.item.title_1}</h2>
									<h1 className="heading mb-style">
										<span className="">{props.item.title_2}</span>
									</h1>
									<h1 className="heading">
										<span className="fill">{props.item.title_3}</span>
										{props.item.title_4}
									</h1>
									<p className="sub-heading">{props.item.description}</p>
									<div className="flat-bt-slider flex style2">
										<Link to="/explore-01" className="sc-button header-slider style style-1 rocket fl-button pri-1">
											<span>Explore</span>
										</Link>
										<Link to="/create-item" className="sc-button header-slider style style-1 note fl-button pri-1">
											<span>Create</span>
										</Link>
									</div>
								</div>
								<div className="image">
									<img className="img-bg" src={props.item.imgbg} alt="Eternal Glorys" />
									<img src={props.item.img} alt="Eternal Glorys" />
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
);
export default SliderStyle1;
