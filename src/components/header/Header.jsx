import clsx from 'clsx';
import LanguageSelection from 'components/LanguageSelection';
import { useEffect, useRef, useState } from 'react';
import { Link, useLocation } from 'react-router-dom';
import { useSelector } from 'store';
import { footerSelector, menuSelector, settingSelector } from 'utils/selectors';
import discordIcon from '../../assets/images/icon/discord-icon.png';
import mediumIcon from '../../assets/images/icon/medium-icon.png';
import telegramIcon from '../../assets/images/icon/telegram-icon.png';
import tiktokIcon from '../../assets/images/icon/tiktok-icon.png';
import twitterIcon from '../../assets/images/icon/twitter-icon.png';
import logoheader from '../../assets/images/logo/logo.svg';

const socialList = [
	{
		icon: discordIcon,
		link: '/discord',
	},
	{
		icon: twitterIcon,
		link: 'https://eternalglory.medium.com/',
	},
	{
		icon: telegramIcon,
		link: '/telegram',
	},
	{
		icon: mediumIcon,
		link: 'https://eternalglory.medium.com/',
	},
	{
		icon: tiktokIcon,
		link: 'https://www.tiktok.com/@eternalglory.io',
	},
];

const Header = () => {
	const { pathname } = useLocation();
	const [isExpand, setIsExpand] = useState(false);
	const [isFixed, setIsFixed] = useState(false);
	const [isSmall, setIsSmall] = useState(false);
	const { menus } = useSelector(menuSelector);
	const { footer } = useSelector(footerSelector);
	const { language: storageLanguage } = useSelector(settingSelector);

	const headerRef = useRef(null);
	useEffect(() => {
		window.addEventListener('scroll', isSticky);
		return () => {
			window.removeEventListener('scroll', isSticky);
		};
	});
	const isSticky = e => {
		const scrollTop = window.scrollY;
		setIsFixed(scrollTop >= 300);
		setIsSmall(scrollTop >= 400);
	};

	const menuLeft = useRef(null);
	const btnToggle = useRef(null);

	const menuToggle = () => {
		setIsExpand(prev => !prev);
	};

	const [activeIndex, setActiveIndex] = useState(0);
	const handleOnClick = (index, link) => {
		if (link.startsWith('#')) {
			setActiveIndex(index);
			const targetSection = document.querySelector(link);
			targetSection.scrollIntoView({
				block: 'start',
				behavior: 'smooth',
			});
		}
		setIsExpand(false);
	};

	return (
		<header
			id="header_main"
			className={clsx('header_1 js-header', {
				'is-fixed': isFixed || isExpand,
				'is-small': isSmall || isExpand,
			})}
			ref={headerRef}
		>
			<div className="themesflat-container">
				<div className="row">
					<div className="col-md-12">
						<div id="site-header-inner">
							<div className="wrap-box flex">
								<div id="site-logo" className="clearfix">
									{isSmall && (
										<div id="site-logo-inner">
											<Link to="/" rel="home" className="main-logo">
												<img
													className="logo-light"
													id="logo_header"
													src={logoheader}
													srcSet={`${logoheader}`}
													alt="nft-gaming"
												/>
											</Link>
										</div>
									)}
								</div>
								<div
									className={clsx('mobile-button', {
										active: isExpand,
									})}
									ref={btnToggle}
									onClick={menuToggle}
								>
									<span></span>
								</div>
								<nav
									id="main-nav"
									className={clsx('main-nav', {
										active: isExpand,
									})}
									ref={menuLeft}
								>
									<div className="nav__content">
										<ul id="menu-primary-menu" className="menu">
											{Object.values(menus.byId)
												.filter(x => !x.isPrimary)
												.map((data, index) => {
													return (
														<li
															key={index}
															onClick={() => handleOnClick(index, data.link)}
															className={clsx(`menu-item ${data.namesub ? 'menu-item-has-children' : ''} `, {
																'hide-on-mobile': data.name[storageLanguage] === 'Home',
																active: activeIndex === index,
															})}
														>
															{data.link.startsWith('http') ? (
																<a href={data.link} target="_blank" rel="noreferrer">
																	{data.name[storageLanguage]}
																</a>
															) : (
																<Link to="/" target={data.link.startsWith('http') ? '_blank' : ''}>
																	{data.name[storageLanguage]}
																</Link>
															)}

															{data.namesub && (
																<ul className="sub-menu">
																	{data.namesub.map(submenu => (
																		<li
																			key={submenu.id}
																			className={pathname === submenu.links ? 'menu-item current-item' : 'menu-item'}
																		>
																			<Link to={submenu.links}>{submenu.sub}</Link>
																		</li>
																	))}
																</ul>
															)}
														</li>
													);
												})}
											<li className="menu-item hide-on-mobile">
												<LanguageSelection />
											</li>
											{Object.values(menus.byId)
												.filter(x => x.isPrimary)
												.map((data, index) => {
													return (
														<li key={index} className="btn-header">
															<Link to={data.link} className="sc-button btn-eg">
																{data.name[storageLanguage]}
															</Link>
														</li>
													);
												})}

											<li className="menu-item hide-on-desktop">
												<LanguageSelection />
											</li>
										</ul>
										<div className="nav__social">
											<span className="nav__social-title">{footer.joinUsOn?.[storageLanguage]}:</span>
											<ul className="nav__social-list">
												{socialList.map((item, index) => (
													<li style={{ float: 'left' }} key={index} className="nav__social-item">
														{item.link.startsWith('https://') ? (
															<a href={item.link} target="_blank" rel="noreferrer">
																<img src={item.icon} alt="social icon" />
															</a>
														) : (
															<Link to={item.link} target="_blank" rel="noopener">
																<img src={item.icon} alt="social icon" />
															</Link>
														)}
													</li>
												))}
											</ul>
										</div>
									</div>
								</nav>
							</div>
						</div>
					</div>
				</div>
			</div>
		</header>
	);
};

export default Header;
