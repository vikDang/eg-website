import adminApi from 'api/adminAPI';
import bannerLogo from 'assets/images/games/banner -logo.png';
import bannerImage from 'assets/images/games/banner-image.png';
import useMounted from 'hooks/useMounted';
import useWindowSize from 'hooks/useResize';
import { useEffect, useRef, useState } from 'react';
import { Link } from 'react-router-dom';
import { useSelector } from 'store';
import { bannerSelector, settingSelector, socialSelector } from 'utils/selectors';

// const socialList = [
// 	{
// 		icon: 'icon-fl-vt',
// 		link: '/discord',
// 	},
// 	{
// 		icon: 'fab fa-twitter',
// 		link: 'https://twitter.com/EternalGlory_io',
// 	},
// 	{
// 		icon: 'fab fa-facebook',
// 		link: 'https://www.facebook.com/eternalgloryio',
// 	},
// 	{
// 		icon: 'fab fa-telegram-plane',
// 		link: '/telegram',
// 	},
// 	{
// 		icon: 'fab fa-youtube',
// 		link: 'https://www.youtube.com/channel/UCiLjXsSzmrEzt0_clnfLiCg',
// 	},
// 	{
// 		icon: 'icon-fl-tik-tok-2',
// 		link: 'https://www.tiktok.com/@eternalglory.io',
// 	},
// 	{
// 		icon: 'fab fa-medium-m',
// 		link: 'https://eternalglory.medium.com/',
// 	},
// ];

const Banner = () => {
	const size = useWindowSize();
	const bannerRef = useRef();
	const mounted = useMounted();

	const { socials } = useSelector(socialSelector);
	const { language: storageLanguage } = useSelector(settingSelector);

	const {
		banners: { topBanner },
	} = useSelector(bannerSelector);

	const handleScroll = () => {
		const { offsetHeight: bannerHeight } = bannerRef.current;
		window.scroll({
			top: bannerHeight,
			left: 0,
			behavior: 'smooth',
		});
	};

	const [bannerMedia, setBannerMedia] = useState(null);
	useEffect(() => {
		const getBannerImage = async imageName => {
			try {
				const bannerMedia = await adminApi.getTopBannerImage({ name: imageName });

				if (mounted.current) {
					setBannerMedia(bannerMedia);
				}
			} catch (error) {
				console.log('The following error occured', error);
			}
		};
		if (topBanner?.backgroundImage) {
			getBannerImage(topBanner.backgroundImage);
		}
	}, [topBanner.backgroundImage]);

	// Create new video source when change banner media
	const [videoURL, setVideoURL] = useState('');
	useEffect(() => {
		if (bannerMedia) setVideoURL(() => URL.createObjectURL(new File([bannerMedia], { type: bannerMedia.type })));
	}, [bannerMedia]);

	const videoRef = useRef();

	useEffect(() => {
		if (videoURL && videoRef.current) {
			videoRef.current.load();
		}
	}, [videoURL]);

	useEffect(() => {
		return () => URL.revokeObjectURL(videoURL);
	}, [videoURL]);

	return (
		<div className="video-banner" ref={bannerRef} id="banner">
			{size.width > 991 ? (
				<video ref={videoRef} loop autoPlay muted playsInline preload="auto">
					<source src={videoURL} type="video/mp4" />
					Your browser does not support the video tag.
				</video>
			) : (
				<div className="banner-box">
					<img src={bannerImage} alt="banner background" style={{ objectFit: 'fill', width: '100%' }} />
				</div>
			)}
			<div className="content">
				{size.width < 991 && <img src={bannerLogo} alt="banner logo" className="banner__logo" />}
				<div className="inner">
					<div className="text-center reveal active fade-bottom">
						<p
							className="des-large-eg shadow"
							dangerouslySetInnerHTML={{
								__html: topBanner.title?.[storageLanguage]?.split('fight for')?.join('fight for<br />') || '',
							}}
						></p>
						<div className="mt-5">
							{/* <Link to="/telegram" className="sc-button btn-eg text-center" target="_blank" rel="noopener">
								Join our Telegram
							</Link>

							<Link to="/discord" className="sc-button btn-eg text-center" target="_blank" rel="noopener">
								Join our Discord
							</Link> */}
							{topBanner.buttons.map(button => (
								<Link
									key={button.id}
									to={button.link}
									className="sc-button btn-eg text-center"
									target="_blank"
									rel="noopener"
								>
									{button.name[storageLanguage]}
								</Link>
							))}
						</div>
					</div>
					<div className="action__container">
						<button className="scroll__btn" onClick={handleScroll}>
							<div className="icon__box">
								<div className="scroll__icon"></div>
							</div>
						</button>
					</div>
				</div>
			</div>
			<div className="banner__social hide-on-mobile">
				<ul className="banner__social-list">
					{Object.values(socials.byId).map(item => (
						<li key={item.id} className="banner__social-item">
							{item.link.startsWith('https://') ? (
								<a href={item.link} target="_blank" rel="noreferrer" className="banner__social-link">
									<i className={item.icon}></i>
								</a>
							) : (
								<Link to={item.link} className="banner__social-link" target="_blank" rel="noopener">
									<i className={item.icon}></i>
								</Link>
							)}
						</li>
					))}
				</ul>
			</div>
		</div>
	);
};

export default Banner;
