import 'animate.css';
import { useEffect, useState } from 'react';
import 'swiper/modules/effect-fade/effect-fade.min.css';
import 'swiper/swiper.scss';

import skillLine from 'assets/images/characters/line-skin.png';
import characterBg from 'assets/images/games/character-bg.png';
import { useGetCharacterImages } from 'hooks/useGetCharacterImages';
import { useMemo } from 'react';
import { useSelector } from 'store';
import 'swiper/modules/navigation/navigation.scss';
import 'swiper/modules/pagination/pagination.scss';
import { characterSelector, settingSelector } from 'utils/selectors';
import SkillDesktop from './components/SkillDesktop';

const CharacterDesktop = () => {
	const [charIndex, setCharIndex] = useState(0);
	const [active, setActive] = useState(false);
	const { language: storageLanguage } = useSelector(settingSelector);

	const characterState = useSelector(characterSelector);
	const characters = useMemo(() => Object.values(characterState.characters.byId), [characterState.characters]);

	useGetCharacterImages(characters);

	useEffect(() => {
		setChar(charIndex);
	}, []);

	const btnNextPrev = (next = true) => {
		let index = next
			? charIndex + 1 > characters.length - 1
				? 0
				: charIndex + 1
			: charIndex - 1 < 0
			? characters.length - 1
			: charIndex - 1;
		setChar(index);
	};

	const setChar = index => {
		setActive(false);
		setTimeout(() => setActive(true), 250);
		setCharIndex(index);
	};

	return (
		<div className="character-box" style={{ backgroundImage: `url(${characterBg})` }}>
			<h2 className="title-eg text-center shadow">{characterState.title?.[storageLanguage]}</h2>
			<div className="row character-icons">
				{characters.map((character, index) => {
					return (
						<div key={character.id} className={`col icon-box ${charIndex === index && 'active'}`}>
							<img
								className="img-icon"
								src={charIndex === index ? character.iconBase64 || '' : character.iconInactiveBase64 || ''}
								alt="icon"
								onClick={() => setChar(index)}
							/>
							<div className="name mt-2">{character.name[storageLanguage]}</div>
						</div>
					);
				})}
			</div>
			<div className="container">
				<div className="hero-slides">
					<div className="button-prev" onClick={() => btnNextPrev(false)}></div>
					<div className="button-next" onClick={() => btnNextPrev()}></div>
					<div className={`hero-box ${active && 'active'}`} id="HeroBox">
						<div className="des-box">
							<h3 className="title-sm-eg shadow">{characters[charIndex]?.name?.[storageLanguage]}</h3>
							<p className="des-large-eg shadow">{characters[charIndex]?.description?.[storageLanguage]}</p>
						</div>
						<div className="character-image text-center">
							<img
								src={characters[charIndex]?.avatarBase64 || ''}
								alt="icon"
								style={{
									transform: (charIndex === 2 || charIndex === 3) && 'scale(1.1) translateY(40px)',
									transformOrigin: 'bottom center',
								}}
							/>
						</div>
						<div className="skills-box">
							<img src={skillLine} alt="skills" />
							<div className="skill-detail">
								{characters[charIndex]?.skills?.map((skill, index) => (
									<SkillDesktop key={skill.id} skill={skill} characterId={characters[charIndex]?.id} />
								))}
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	);
};

export default CharacterDesktop;
