import { useGetSkillIcons } from 'hooks/useGetSkillIcons';
import { useSelector } from 'store';
import { settingSelector } from 'utils/selectors';

function SkillDesktop(props) {
	const { skill, characterId } = props;
	useGetSkillIcons(skill, characterId);
	const { language: storageLanguage } = useSelector(settingSelector);

	return (
		<div className="skill-item">
			<img src={skill.iconBase64 || ''} alt="" />
			<span>{skill.info[storageLanguage]}</span>
		</div>
	);
}

export default SkillDesktop;
