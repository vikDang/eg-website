import React, { useEffect, useRef, useState } from 'react';
import HeroImage from './HeroImage';
import PropTypes from 'prop-types';

CharacterHero.propTypes = {
	canShowImage: PropTypes.bool.isRequired,
	imgSource: PropTypes.string.isRequired,
	index: PropTypes.number.isRequired,
	isZeroBottom: PropTypes.bool,
};

CharacterHero.defaultProps = {
	isZeroBottom: false,
};

function CharacterHero(props) {
	const { canShowImage } = props;
	const imgBoxRef = useRef();

	return (
		<div className="character-image text-center">
			<div className="character__img" ref={imgBoxRef}>
				<div className="character__img-aura"></div>
				{canShowImage && <HeroImage {...props} boxElement={imgBoxRef.current} />}
			</div>
		</div>
	);
}

export default CharacterHero;
