import { useGetSkillIcons } from 'hooks/useGetSkillIcons';
import { useSelector } from 'store';
import { settingSelector } from 'utils/selectors';

function SkillMobile(props) {
	const { skill, characterId } = props;

	useGetSkillIcons(skill, characterId);
	const { language: storageLanguage } = useSelector(settingSelector);

	return (
		<div className="skill-item">
			<img src={skill.iconBase64 || ''} alt={skill.info[storageLanguage]} />
		</div>
	);
}

export default SkillMobile;
