import React, { useEffect, useRef, useState } from 'react';
import PropTypes from 'prop-types';

HeroImage.propTypes = {
	canShowImage: PropTypes.bool.isRequired,
	imgSource: PropTypes.string.isRequired,
	isZeroBottom: PropTypes.bool,
	index: PropTypes.number.isRequired,
	boxElement: PropTypes.object.isRequired,
};

HeroImage.defaultProps = {
	isZeroBottom: false,
};

function HeroImage(props) {
	const { imgSource, isZeroBottom = false, boxElement, index } = props;
	const [isScaling, setIsScaling] = useState(true);
	const bottomValue = 15;

	const [scaleRatio, setScaleRatio] = useState(1);
	const heroImgRef = useRef();

	const handleResizeImg = imageElement => {
		const { offsetHeight: boxHeight } = boxElement;
		const { offsetHeight: imgHeight } = imageElement;
		const maxHeight = boxHeight - (boxHeight * bottomValue) / 100;

		let scaleRatio = imgHeight > maxHeight ? maxHeight / imgHeight : 1;

		switch (index) {
			case 2:
				scaleRatio = scaleRatio + (1 - scaleRatio) / 100;
				break;
			case 3:
				scaleRatio = scaleRatio + (1 - scaleRatio) / 4;
				break;
			default:
		}

		setScaleRatio(scaleRatio);
		setIsScaling(false);
	};

	useEffect(() => {
		const heroImage = heroImgRef.current;

		const timerId = setInterval(() => {
			if (heroImage.naturalHeight > 0) {
				handleResizeImg(heroImage);
				clearInterval(timerId);
			}
		}, 0);
		return () => {
			clearInterval(timerId);
		};
	}, [imgSource]);

	return (
		<img
			className="character__img-hero"
			src={imgSource}
			alt="icon"
			ref={heroImgRef}
			style={{
				transform: isZeroBottom
					? `translateX(-50%) scale(${scaleRatio + (1 - scaleRatio) / 3})`
					: `translateX(-50%) scale(${scaleRatio})`,
				bottom: isZeroBottom ? 0 : `${bottomValue}%`,
				opacity: !isScaling && 1,
			}}
		/>
	);
}

export default HeroImage;
