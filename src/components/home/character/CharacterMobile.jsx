import 'animate.css';
import clsx from 'clsx';
import { useEffect, useMemo, useRef, useState } from 'react';
import { Navigation, Pagination } from 'swiper';

import 'swiper/modules/effect-fade/effect-fade.min.css';
import { Swiper, SwiperSlide } from 'swiper/react/swiper-react';
import 'swiper/swiper.scss';

import { useGetCharacterImages } from 'hooks/useGetCharacterImages';
import { useSelector } from 'store';
import 'swiper/modules/navigation/navigation.scss';
import 'swiper/modules/pagination/pagination.scss';
import { characterSelector, settingSelector } from 'utils/selectors';
import CharacterHero from './components/CharacterHero';
import SkillMobile from './components/SkillMobile';

const CharacterMobile = () => {
	const [charIndex, setCharIndex] = useState(0);
	const [active, setActive] = useState(false);
	const timerId = useRef();
	const { language: storageLanguage } = useSelector(settingSelector);

	const characterState = useSelector(characterSelector);
	const characters = useMemo(() => Object.values(characterState.characters.byId), [characterState.characters]);
	useGetCharacterImages(characters);

	useEffect(() => {
		setChar(charIndex);

		return () => clearTimeout(timerId.current);
	}, []);

	const handleNextSlide = () => {
		const swiper = document.querySelector('.character__icon-list').swiper;
		swiper.slideNext();
	};

	const handlePrevSlide = () => {
		const swiper = document.querySelector('.character__icon-list').swiper;
		swiper.slidePrev();
	};

	const setChar = index => {
		if (timerId.current) clearTimeout(timerId.current);
		setActive(false);
		timerId.current = setTimeout(() => setActive(true), 250);
		setCharIndex(index);
	};

	const handleChangeActiveSlide = swiper => {
		setChar(swiper.realIndex);
	};

	const handleClickCharacterIcon = (index, activeIndex) => {
		const swiper = document.querySelector('.character__icon-list').swiper;

		const isNext = index - activeIndex === 1 || (index === 0 && activeIndex === characters.length - 1);
		const isPrev = index - activeIndex === -1 || (index === characters.length - 1 && activeIndex === 0);

		if (isNext) {
			swiper.slideNext();
		} else if (isPrev) {
			swiper.slidePrev();
		}
	};

	return (
		<div className="character__box">
			<div className="character__bg"></div>
			<div className="hero__actions">
				<div className="button-prev" onClick={handlePrevSlide}>
					<div className="button__prev-img"></div>
				</div>
				<div className="button-next" onClick={handleNextSlide}>
					<div className="button__next-img"></div>
				</div>
			</div>
			<div className="character__container">
				<h2 className="title-eg text-center shadow">{characterState.title?.[storageLanguage]}</h2>
				<Swiper
					modules={[Navigation, Pagination]}
					navigation={true}
					slidesPerView={3}
					slidesPerGroup={1}
					loop={true}
					loopFillGroupWithBlank={true}
					scrollbar={{ draggable: false }}
					centeredSlides={true}
					onActiveIndexChange={handleChangeActiveSlide}
					className="character__icon-list"
				>
					{characters.map((character, index) => (
						<SwiperSlide key={index} className={clsx(character.class, 'slide-item')}>
							<div
								className={`character__icon-item ${charIndex === index && 'active'}`}
								key={'icon-' + index}
								onClick={() => handleClickCharacterIcon(index, charIndex)}
							>
								<img
									className="img-icon"
									src={charIndex === index ? character.iconBase64 || '' : character.iconInactiveBase64 || ''}
									alt="icon"
								/>
								<div className="name mt-2">{characters[charIndex]?.name?.[storageLanguage]}</div>
							</div>
						</SwiperSlide>
					))}
				</Swiper>
				<div className="hero-slides">
					<div className={`hero-box ${active && 'active'}`} id="HeroBox">
						<CharacterHero
							imgSource={characters[charIndex]?.avatarBase64 || ''}
							canShowImage={active}
							isZeroBottom={charIndex === 2 || charIndex === 3}
							index={charIndex}
						/>
						<div className="skills-box">
							<div className="skill-detail">
								{characters[charIndex]?.skills?.map((skill, index) => (
									<SkillMobile key={skill.id} skill={skill} characterId={characters[charIndex]?.id} />
								))}
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	);
};

export default CharacterMobile;
