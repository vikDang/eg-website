import 'animate.css';
import clsx from 'clsx';
import { useGetCharacterImages } from 'hooks/useGetCharacterImages';
import { useEffect, useMemo, useRef, useState } from 'react';
import { useSelector } from 'store';
import 'swiper/modules/effect-fade/effect-fade.min.css';
import 'swiper/modules/navigation/navigation.scss';
import 'swiper/modules/pagination/pagination.scss';
import 'swiper/swiper.scss';
import { characterSelector, settingSelector } from 'utils/selectors';
import CharacterHero from './components/CharacterHero';
import SkillDesktop from './components/SkillDesktop';

const CharacterTablet = () => {
	const [charIndex, setCharIndex] = useState(1);
	const [active, setActive] = useState(false);
	const timerId = useRef();
	const { language: storageLanguage } = useSelector(settingSelector);

	const characterState = useSelector(characterSelector);
	const characters = useMemo(() => Object.values(characterState.characters.byId), [characterState.characters]);
	useGetCharacterImages(characters);

	useEffect(() => {
		setChar(charIndex);

		return () => clearTimeout(timerId.current);
	}, []);

	const btnNextPrev = (next = true) => {
		let index = next
			? charIndex + 1 > characters.length - 1
				? 0
				: charIndex + 1
			: charIndex - 1 < 0
			? characters.length - 1
			: charIndex - 1;
		setChar(index);
	};

	const setChar = index => {
		if (timerId.current) clearTimeout(timerId.current);
		setActive(false);
		timerId.current = setTimeout(() => setActive(true), 250);
		setCharIndex(index);
	};

	return (
		<div className="character__box">
			<div className="character__bg"></div>
			<div className="character__container">
				<div className="hero__actions">
					<div className="button-prev" onClick={() => btnNextPrev(false)}>
						<div className="button__prev-img"></div>
					</div>
					<div className="button-next" onClick={() => btnNextPrev()}>
						<div className="button__next-img"></div>
					</div>
				</div>
				<h2 className="title-eg text-center shadow">{characterState.title?.[storageLanguage]}</h2>
				<div className="character__icon-list">
					{characters.map((character, index) => {
						return (
							<div key={character.id} className={`character__icon-item ${charIndex === index && 'active'}`}>
								<img
									className="img-icon"
									src={charIndex === index ? character.iconBase64 || '' : character.iconInactiveBase64 || ''}
									alt="icon"
									onClick={() => setChar(index)}
								/>
								<div className="name mt-2">{character.name[storageLanguage]}</div>
							</div>
						);
					})}
				</div>
				<div className="hero-slides">
					<div
						className={clsx('hero-box', {
							active: active,
						})}
						id="HeroBox"
					>
						<div className="des-box">
							<h3 className="title-sm-eg shadow">{characters[charIndex]?.name?.[storageLanguage]}</h3>
							<div className="info__line"></div>
							<p className="des-large-eg shadow">{characters[charIndex]?.description?.[storageLanguage]}</p>
						</div>
						<CharacterHero
							imgSource={characters[charIndex]?.avatarBase64 || ''}
							canShowImage={active}
							isZeroBottom={charIndex === 2 || charIndex === 3}
							index={charIndex}
						/>

						{/* <div className="character-image text-center">
							<div className="character__img">
								<div className="character__img-aura"></div>
								<img className="character__img-hero" src={characters[charIndex]?.img?} alt="icon" />
							</div>
						</div> */}
						<div className="skills-box">
							<div className="line__container">
								<div className="skill__line"></div>
							</div>
							<div className="skill-detail">
								{characters[charIndex]?.skills?.map((skill, index) => (
									<SkillDesktop key={skill.id} skill={skill} characterId={characters[charIndex]?.id} />
								))}
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	);
};

export default CharacterTablet;
