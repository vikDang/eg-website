import adminApi from 'api/adminAPI';
import videoBg from 'assets/videoBg/bottom-video.mp4';
import useMounted from 'hooks/useMounted';
import { useEffect, useLayoutEffect, useRef, useState } from 'react';
import { useSelector } from 'store';
import { bannerSelector } from 'utils/selectors';

const VideoBg = () => {
	const mounted = useMounted();

	const {
		banners: { bottomBanner },
	} = useSelector(bannerSelector);

	const [bannerMedia, setBannerMedia] = useState(null);
	useLayoutEffect(() => {
		const getBannerImage = async imageName => {
			try {
				const bannerMedia = await adminApi.getBottomBannerImage({ name: imageName });

				if (mounted.current) {
					setBannerMedia(bannerMedia);
				}
			} catch (error) {
				console.log('The following error occured', error);
			}
		};
		if (bottomBanner?.backgroundImage) {
			getBannerImage(bottomBanner.backgroundImage);
		}
	}, [bottomBanner.backgroundImage]);

	// Create new video source when change banner media
	const [videoURL, setVideoURL] = useState('');
	useEffect(() => {
		if (bannerMedia) setVideoURL(() => URL.createObjectURL(new File([bannerMedia], { type: 'video/mp4' })));
	}, [bannerMedia]);

	const videoRef = useRef();

	useEffect(() => {
		if (videoURL && videoRef.current) {
			videoRef.current.load();
		}
	}, [videoURL]);

	useEffect(() => {
		return () => URL.revokeObjectURL(videoURL);
	}, [videoURL]);
	return (
		<div className="video-bg-box">
			<video ref={videoRef} loop autoPlay muted playsInline preload="auto">
				<source src={videoURL} type="video/mp4" />
				Your browser does not support the video tag.
			</video>
			<div className="border"></div>
		</div>
	);
};

export default VideoBg;
