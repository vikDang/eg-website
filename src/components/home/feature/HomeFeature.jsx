import React from 'react';
import YouTube from 'react-youtube';

import useWindowSize from 'hooks/useResize';
import FeatureMobile from './FeatureMobile';
import FeatureDesktop from './FeatureDesktop';
import { useSelector } from 'store';
import { settingSelector, trailerSelector } from 'utils/selectors';

const HomeFeature = () => {
	const opts = {
		playerVars: {
			// https://developers.google.com/youtube/player_parameters
			autoplay: 0,
		},
		preload: 'auto',
	};
	const { trailer } = useSelector(trailerSelector);
	const { language: storageLanguage } = useSelector(settingSelector);

	const size = useWindowSize();

	return (
		<>
			<div className="video-box">
				<div className="video-frame"></div>
				<YouTube videoId={trailer.trailer[storageLanguage]} opts={opts} />
			</div>
			{size.width > 991 ? <FeatureDesktop /> : <FeatureMobile />}
		</>
	);
};

export default HomeFeature;
