import { useGetAboutImages } from 'hooks/useGetAboutImages';
import { useMemo } from 'react';
import { Tab, TabList, TabPanel, Tabs } from 'react-tabs';
import { useSelector } from 'store';
import { aboutSelector, settingSelector } from 'utils/selectors';

const FeatureMobile = () => {
	const { abouts } = useSelector(aboutSelector);

	const featureFirst = useMemo(() => Object.values(abouts.byId)[0], [abouts]);
	const featureSecond = useMemo(() => Object.values(abouts.byId)[1], [abouts]);
	const featureThird = useMemo(() => Object.values(abouts.byId)[2], [abouts]);
	const { language: storageLanguage } = useSelector(settingSelector);

	useGetAboutImages(featureFirst);
	useGetAboutImages(featureSecond);
	useGetAboutImages(featureThird);

	return (
		<div className="about__container">
			<div className="scroll-to__area" id="feature"></div>

			<div className="about__mobile-bg"></div>

			<Tabs className="feature-tabs">
				<TabList>
					<Tab>
						<img src={featureFirst.imageIconBase64 || ''} alt="" />
					</Tab>
					<Tab>
						<img src={featureSecond.imageIconBase64 || ''} alt="" />
					</Tab>
					<Tab>
						<img src={featureThird.imageIconBase64 || ''} alt="" />
					</Tab>
				</TabList>

				<TabPanel className="text-center">
					<h2 className="title-eg text-center">{featureFirst.title[storageLanguage]}</h2>
					<p className="des-large-eg text-center">{featureFirst.content[storageLanguage]}</p>
					<div className="multiPlay__container--image">
						<div
							className="multiPlay__img--mobile"
							style={{
								background: `url('${featureFirst.imageBase64 || ''}') no-repeat center center / contain`,
							}}
						></div>
					</div>
				</TabPanel>
				<TabPanel className="text-center">
					<h2 className="title-eg text-center">{featureSecond.title[storageLanguage]}</h2>
					<p className="des-large-eg text-center">{featureSecond.content[storageLanguage]}</p>
					<div className="higherJob__container--mobile">
						<div
							className="higherJob__img--mobile"
							style={{
								background: `url('${featureSecond.imageBase64 || ''}') no-repeat center center / contain`,
							}}
						></div>
					</div>
				</TabPanel>
				<TabPanel className="text-center">
					<h2 className="title-eg text-center">{featureThird.title[storageLanguage]}</h2>
					<p className="des-large-eg text-center">{featureThird.content[storageLanguage]}</p>
					<div className="land__container--mobile">
						<div
							className="land__img--mobile"
							style={{
								background: `url('${featureThird.imageBase64 || ''}') no-repeat center center / contain`,
							}}
						></div>
					</div>
				</TabPanel>
			</Tabs>
		</div>
	);
};

export default FeatureMobile;
