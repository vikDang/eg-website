import { useGetTokenImages } from 'hooks/useGetTokenImages';
import useWindowSize from 'hooks/useResize';
import { useSelector } from 'store';
import { settingSelector, tokenSelector } from 'utils/selectors';

const About = () => {
	const size = useWindowSize();
	const {
		token,
		imageBase64: tokenImageBase64,
		imageMobileBase64: tokenImageMobileBase64,
	} = useSelector(tokenSelector);
	const { language: storageLanguage } = useSelector(settingSelector);
	useGetTokenImages(token, storageLanguage);

	return (
		<div className="text-center about--elo">
			<div className="scroll-to__area" id="about-elo"></div>

			<h2 className="title-eg shadow">{token.title?.[storageLanguage]}</h2>
			{size.width > 991 ? (
				<>
					<p className="des-large-eg reveal fade-bottom" style={{ display: 'inline-block', width: '86.6%' }}>
						{token.description?.[storageLanguage]}
					</p>
					<img
						className="img-fluid reveal fade-bottom"
						src={tokenImageBase64 || ''}
						alt="Multiplay"
						style={{ marginTop: '-12vw', width: '100%' }}
					/>
				</>
			) : (
				<img className="img-fluid" src={tokenImageMobileBase64} alt="Multiplay" style={{ marginTop: '-16vw' }} />
			)}
		</div>
	);
};

export default About;
