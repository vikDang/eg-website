import { useGetAboutImages } from 'hooks/useGetAboutImages';
import { useMemo } from 'react';
import { useSelector } from 'store';
import { aboutSelector, settingSelector } from 'utils/selectors';
import pointIcon from 'assets/images/icon/point.png';

const FeatureDesktop = () => {
	const { abouts } = useSelector(aboutSelector);

	const featureFirst = useMemo(() => Object.values(abouts.byId)[0], [abouts]);
	const featureSecond = useMemo(() => Object.values(abouts.byId)[1], [abouts]);
	const featureThird = useMemo(() => Object.values(abouts.byId)[2], [abouts]);
	const { language: storageLanguage } = useSelector(settingSelector);

	useGetAboutImages(featureFirst);
	useGetAboutImages(featureSecond);
	useGetAboutImages(featureThird);

	return (
		<div className="about__container">
			<div className="scroll-to__area" id="feature"></div>
			<div className="about__main-bg"></div>
			<div className="about__section-1">
				<div className="row ft-1 no-gutters">
					<div className="col-md-6 reveal fade-left">
						<div
							className="about__section-container"
							style={{ marginLeft: '3.1017vw', marginTop: '4.714vw', marginRight: '2.2vw' }}
						>
							<img src={featureFirst.imageIconBase64 || ''} className="about-icon" alt="" />
							<div className="about__content">
								<h2 className="title-eg text-uppercase">{featureFirst.title[storageLanguage]}</h2>
								<p className="des-large-eg">{featureFirst.content[storageLanguage]}</p>
							</div>
						</div>
					</div>
					<div className="col-md-6 reveal fade-right text-center">
						<div className="multiPlay__container">
							<div
								className="multiPlay__img"
								style={{
									background: `url('${featureFirst.imageBase64 || ''}') no-repeat center center / contain`,
								}}
							></div>
						</div>
					</div>
				</div>
			</div>

			<div className="about__container--sub">
				<div className="about__sub-bg"></div>
				<div className="about__section-2">
					<div className="row ft-2 no-gutters">
						<div className="col-md-6 reveal fade-left text-center">
							<div className="higherJob__container">
								<div
									className="higherJob__img"
									style={{
										background: `url('${featureSecond.imageBase64 || ''}') no-repeat center center / contain`,
									}}
								></div>
							</div>
						</div>
						<div className="col-md-6 reveal fade-right">
							<div
								className="about__section-container"
								style={{ marginLeft: '-2.2333vw', marginTop: '3.4739vw', marginRight: '8vw' }}
							>
								<img src={featureSecond.imageIconBase64 || ''} className="about-icon" alt="" />
								<div className="about__content">
									<h2 className="title-eg text-uppercase">{featureSecond.title[storageLanguage]}</h2>
									<p className="des-large-eg">{featureSecond.content[storageLanguage]}</p>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div className="about__section-3">
					<div className="row ft-3 no-gutters">
						<div className="col-md-6 reveal fade-left">
							<div className="about__section-container" style={{ marginLeft: '2.9777vw', marginRight: '2vw' }}>
								<img src={featureThird.imageIconBase64 || ''} className="about-icon" alt="" />
								<div className="about__content">
									<h2 className="title-eg text-uppercase">{featureThird.title[storageLanguage]}</h2>
									<p className="des-large-eg">{featureThird.content[storageLanguage]}</p>
								</div>
							</div>
						</div>
						<div className="col-md-6 reveal fade-right text-center">
							<div className="land__container">
								<div
									className="land__img"
									style={{
										background: `url('${featureThird.imageBase64 || ''}') no-repeat center center / contain`,
									}}
								></div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<img className="point" src={pointIcon} alt="point" />
			<img className="point right" src={pointIcon} alt="point" />
		</div>
	);
};

export default FeatureDesktop;
