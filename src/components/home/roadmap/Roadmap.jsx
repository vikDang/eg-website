import { useGetRoadmapImages } from 'hooks/useGetRoadmapImages';
import { useSelector } from 'store';
import { roadmapSelector, settingSelector } from 'utils/selectors';

const Roadmap = () => {
	const { roadmap, contentImageBase64, topImageBase64, bottomImageBase64 } = useSelector(roadmapSelector);
	const { language: storageLanguage } = useSelector(settingSelector);

	useGetRoadmapImages(roadmap, storageLanguage);

	return (
		<div className="roadmap-inner">
			<div className="roadmap__container">
				<div className="roadmap__container-bg"></div>
				<div className="roadmap__container-content">
					<h2 className="title-eg text-uppercase shadow">{roadmap.title?.[storageLanguage]}</h2>
					{/* <RoadmapList dataList={roadmapData} /> */}
					<div className="roadmap__img-box reveal fade-bottom">
						<div
							className="roadmap__img"
							style={{ background: `url(${contentImageBase64 || ''}) no-repeat center center / contain` }}
						></div>
					</div>
				</div>
				<img className="top-img reveal fade-left" src={topImageBase64 || ''} alt="Multiplay" />
				<img className="bottom-img reveal fade-right" src={bottomImageBase64 || ''} alt="Multiplay" />
			</div>
		</div>
	);
};

export default Roadmap;
