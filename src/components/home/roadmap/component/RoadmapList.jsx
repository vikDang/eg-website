import RoadMapItem from './RoadmapItem';

const RoadmapList = props => {
	const { dataList } = props;
	return (
		<div className="roadmap__graph">
			{dataList.map((item, index) => (
				<div key={index} className="roadmap__box">
					<RoadMapItem isFinal={index === dataList.length - 1} isFirst={index === 0} isPassed={index < 2} item={item} />
					<span className="roadmap__pad" style={{ height: `${1.875 * index}vw` }}></span>
				</div>
			))}
		</div>
	);
};

export default RoadmapList;
