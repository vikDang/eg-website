import diamondIcon from 'assets/images/icon/diamond_icon.png';
import lightDiamondIcon from 'assets/images/icon/ligth-diamond.png';
import darkDiamondIcon from 'assets/images/icon/dark-diamond.png';
import headArrowIcon from 'assets/images/icon/head-arrow.png';
import clsx from 'clsx';
import { useSelector } from 'store';
import { settingSelector } from 'utils/selectors';

const RoadMapItem = props => {
	const { item, isFirst = false, isFinal = false, isPassed = false } = props;
	const { language: storageLanguage } = useSelector(settingSelector);

	return (
		<div className="roadmap__item">
			<div className="roadmap__right">
				<span className="circle-line hide-on-mobile"></span>
				<img className="diamond-icon hide-on-mobile" src={diamondIcon} alt="Diamon icon" />
				{isFirst && (
					<span
						className={clsx('hook-line--mobile', {
							first: isFirst,
							last: isFinal,
							passed: isPassed,
						})}
					></span>
				)}
				<img
					className={clsx('diamond-icon--mobile')}
					src={isPassed ? lightDiamondIcon : darkDiamondIcon}
					alt="Diamon icon"
				/>
				<span
					className={clsx('circle-line--mobile', {
						passed: isPassed,
					})}
					style={{ visibility: isFinal ? 'hidden' : '' }}
				></span>
				{isFinal && (
					<>
						<span
							className={clsx('hook-line--mobile', {
								first: isFirst,
								last: isFinal,
								passed: isPassed,
							})}
						>
							<div className="head-arrow">
								<img src={headArrowIcon} alt="head arrow" />
							</div>
						</span>
					</>
				)}
			</div>
			<div className="roadmap__left" style={{ marginBottom: isFinal ? 0 : '' }}>
				<div className="roadmap__header">
					<h3 className="roadmap__title">{item.header[storageLanguage]}</h3>
				</div>
				<div className="roadmap__content">
					<ul className="roadmap__list">
						{item.todo.map((todo, index) => (
							<li key={index} className="roadmap__list-item">
								<span>{todo[storageLanguage]}</span>
							</li>
						))}
					</ul>
				</div>
				<div className="roadmap__footer">
					<span className="roadmap__curve" style={{ visibility: isFinal ? 'hidden' : '' }}></span>
				</div>
			</div>
		</div>
	);
};

export default RoadMapItem;
