import { Tab, TabList, TabPanel, Tabs } from 'react-tabs';

import dragonImg from 'assets/images/games/dragon.png';
import { useGetRoadmapImages } from 'hooks/useGetRoadmapImages';
import { useSelector } from 'store';
import { roadmapSelector, settingSelector } from 'utils/selectors';
import RoadmapList from './component/RoadmapList';

const RoadmapMobile = () => {
	const { roadmap } = useSelector(roadmapSelector);
	const { language: storageLanguage } = useSelector(settingSelector);

	useGetRoadmapImages(roadmap, storageLanguage);
	return (
		<div className="roadmap-inner">
			<div className="roadmap__container">
				<div className="roadmap__container-bg"></div>
				<div className="roadmap__container-content">
					<img className="top-img" src={dragonImg} alt="Multiplay" />
					<h2 className="title-eg text-uppercase shadow">{roadmap.title?.[storageLanguage]}</h2>
					<Tabs className="roadmap-tabs" selectedTabClassName="active">
						<TabList>
							{roadmap.info.map(info => (
								<Tab key={info.id} className="roadmap__tab-item">
									<div className="btn-tab">{info.year}</div>
								</Tab>
							))}
						</TabList>

						{roadmap.info.map(info => (
							<TabPanel key={info.id}>
								<RoadmapList dataList={info.detail} />
							</TabPanel>
						))}
						{/* <TabPanel>
							<RoadmapList dataList={data2023} />
						</TabPanel> */}
					</Tabs>
				</div>
			</div>
		</div>
	);
};

export default RoadmapMobile;
