import { useGetInvestorImage } from 'hooks/useGetInvestorImage';
import { useRef } from 'react';

const InvestorImage = props => {
	const { investor } = props;
	useGetInvestorImage(investor);
	const imageRef = useRef();

	return (
		<img
			src={investor.imageBase64}
			alt="eg admin"
			className="investor__image"
			style={{
				display: 'block',
			}}
			ref={imageRef}
		/>
	);
};

export default InvestorImage;
