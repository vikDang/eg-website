import React, { useMemo } from 'react';
import pointIcon from 'assets/images/icon/point.png';
import { useSelector } from 'store';
import { investorSelector, settingSelector } from 'utils/selectors';
import InvestorImage from './InvestorImage';

const MIN_INVESTOR_LENGTH = 12;

const Investor = () => {
	const investorState = useSelector(investorSelector);
	const { language: storageLanguage } = useSelector(settingSelector);

	const comingSoons = useMemo(() => {
		const listTextLength =
			investorState.investors.allIds.length < MIN_INVESTOR_LENGTH
				? MIN_INVESTOR_LENGTH - investorState.investors.allIds.length
				: 0;

		return new Array(listTextLength).fill(investorState.comingSoonText?.[storageLanguage]);
	}, [investorState.investors.allIds, investorState.comingSoonText, storageLanguage]);

	return (
		<div className="investor-box">
			<h2 className="title-eg text-center shadow">{investorState.title?.[storageLanguage]}</h2>
			<div className="row investor-list reveal fade-bottom">
				{Object.values(investorState.investors.byId).map((investor, index) => {
					return (
						<div
							key={index}
							className="col-6 col-lg-3 text-center item"
							style={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}
						>
							<InvestorImage investor={investor} />
						</div>
					);
				})}
				{comingSoons.map((comingSoonText, index) => {
					return (
						<div
							key={index}
							className={`col-6 col-lg-3 text-center item ${
								index > comingSoons.length - 6 - 1 && comingSoonText.length > 6 ? 'hide-on-mobile' : ''
							}`}
							style={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}
						>
							<h3 className="title-eg investor-text">{comingSoonText}</h3>
						</div>
					);
				})}
			</div>
			<img className="point hide-on-mobile" src={pointIcon} alt="point" />
			<img className="point right hide-on-mobile" src={pointIcon} alt="point" />
		</div>
	);
};

export default Investor;
