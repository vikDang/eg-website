import clsx from 'clsx';
import { useEffect, useMemo, useState } from 'react';
import layoutSlice from 'slices/settings';
import { useDispatch, useSelector } from 'store';
import { configLanguage } from 'utils/config';
import { languageSelector, settingSelector } from 'utils/selectors';
import './LanguageSelection.scss';

const LanguageSelection = props => {
	const settings = useSelector(settingSelector);
	const dispatch = useDispatch();

	const [open, setOpen] = useState(false);

	const handleClose = () => {
		setOpen(false);
	};
	const { languages } = useSelector(languageSelector);
	const languageCodeFull = useMemo(() => {
		return Object.values(languages.byId);
	}, [languages.byId]);
	const [language, setLanguage] = useState(() => {
		let storageLanguage = configLanguage.get() || 'en';
		const defaultLanguage = languageCodeFull.find(x => x.code === storageLanguage);
		configLanguage.set(storageLanguage);
		const firstLanguage = languages.byId[languages.allIds[0]];
		return defaultLanguage || firstLanguage;
	});

	const handleChangeLanguage = languageCode => {
		configLanguage.set(languageCode);
		const newLanguage = languageCodeFull.find(x => x.code === languageCode);
		setLanguage(newLanguage);

		setOpen(false);
	};

	useEffect(() => {
		const newSetting = { ...settings, language: language.code };
		dispatch(layoutSlice.actions.save(newSetting));
	}, [language]);

	useEffect(() => {
		window.addEventListener('click', handleClose);

		return () => {
			window.removeEventListener('click', handleClose);
		};
	});

	// List language not include current language
	const remainingLanguages = languageCodeFull.filter(lang => lang.code !== language.code);

	return (
		<div className="language">
			<div
				className="language__current"
				onClick={e => {
					e.stopPropagation();
					setOpen(prev => !prev);
				}}
			>
				<div className="language__left">
					<img src={language.icon} alt="" className="language__image" />
				</div>
				<div className="language__info">
					<span className="language__name">{language.name}</span>
					<i className="fas fa-chevron-down language__icon"></i>
				</div>
			</div>
			<ul
				className={clsx('language__list', {
					open: open,
				})}
			>
				{remainingLanguages.map(lang => {
					return (
						<li
							key={lang.code}
							className="language__item"
							onClick={e => {
								// e.stopPropagation();
								handleChangeLanguage(lang.code);
							}}
						>
							<div className="language__left">
								<img src={lang.icon} alt="" className="language__image" />
							</div>
							<div className="language__info">
								<span className="language__name">{lang.name}</span>
							</div>
						</li>
					);
				})}
			</ul>
		</div>
	);
};

export default LanguageSelection;
