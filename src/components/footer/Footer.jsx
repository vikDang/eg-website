import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { useSelector } from 'store';
import { footerSelector, settingSelector, socialSelector } from 'utils/selectors';
import footerBg from '../../assets/images/games/footer-bg.png';
import logoSvg from '../../assets/images/logo/logo.svg';

const Footer = () => {
	// const socialList = [
	// 	{
	// 		icon: 'fab fa-twitter',
	// 		link: 'https://twitter.com/EternalGlory_io',
	// 	},
	// 	{
	// 		icon: 'fab fa-facebook',
	// 		link: 'https://www.facebook.com/eternalgloryio',
	// 	},
	// 	{
	// 		icon: 'fab fa-telegram-plane',
	// 		link: '/telegram',
	// 	},
	// 	{
	// 		icon: 'fab fa-youtube',
	// 		link: 'https://www.youtube.com/channel/UCiLjXsSzmrEzt0_clnfLiCg',
	// 	},
	// 	{
	// 		icon: 'icon-fl-tik-tok-2',
	// 		link: 'https://www.tiktok.com/@eternalglory.io',
	// 	},
	// 	{
	// 		icon: 'icon-fl-vt',
	// 		link: '/discord',
	// 	},
	// ];

	const [isVisible, setIsVisible] = useState(false);
	const { socials } = useSelector(socialSelector);
	const { footer } = useSelector(footerSelector);
	const { language: storageLanguage } = useSelector(settingSelector);

	const scrollToTop = () => {
		window.scrollTo({
			top: 0,
			behavior: 'smooth',
		});
	};

	useEffect(() => {
		const toggleVisibility = () => {
			if (window.pageYOffset > 500) {
				setIsVisible(true);
			} else {
				setIsVisible(false);
			}
		};

		window.addEventListener('scroll', toggleVisibility);

		return () => window.removeEventListener('scroll', toggleVisibility);
	}, []);

	return (
		<div>
			<footer id="footer" style={{ backgroundImage: `url(${footerBg})` }}>
				<div className="text-center logo">
					<img className="img-fluid" src={logoSvg} alt="logo" />
				</div>
				<p className="des-large-eg text-center mt-5">{footer.description?.[storageLanguage]}</p>
				<div className="container widget widget-subcribe">
					<div className="form-subcribe">
						<div id="mc_embed_signup">
							<form
								action="https://gmail.us18.list-manage.com/subscribe/post?u=87cd4b3fa1fc98b0265b77462&amp;id=052818efc6"
								method="post"
								id="mc-embedded-subscribe-form"
								name="mc-embedded-subscribe-form"
								className="validate form-submit"
								target="_blank"
								noValidate
							>
								<div id="mc_embed_signup_scroll" className="form__container">
									<div className="indicates-required" style={{ display: 'none' }}>
										<span className="asterisk">*</span> indicates required
									</div>
									<div className="mc-field-group">
										<label htmlFor="mce-EMAIL" style={{ display: 'none' }}>
											Email Address <span className="asterisk">*</span>
										</label>

										<input type="email" name="EMAIL" className="required email subscribeEmail" id="mce-EMAIL" />
									</div>
									<div id="mce-responses" className="clear foot" style={{ display: 'none' }}>
										<div className="response" id="mce-error-response" style={{ display: 'none' }}></div>
										<div className="response" id="mce-success-response" style={{ display: 'none' }}></div>
									</div>
									<div style={{ position: 'absolute', left: '-5000px', display: 'none' }} aria-hidden="true">
										<input type="text" name="b_87cd4b3fa1fc98b0265b77462_052818efc6" tabIndex="-1" />
									</div>
									<div className="optionalParent" style={{ height: '100%' }}>
										<div className="clear foot" style={{ height: '100%' }}>
											<input
												type="submit"
												value={footer.buttonContent?.[storageLanguage]}
												name="subscribe"
												id="mc-embedded-subscribe"
												className="button"
											/>
										</div>
									</div>
								</div>
							</form>
						</div>

						{/* <!--End mc_embed_signup--> */}
					</div>
					<h2 className="title-eg text-center text-white mt-5">{footer.followUs?.[storageLanguage]}:</h2>
					<div style={{ textAlign: 'center' }} className="widget-social style-1 mg-t30">
						<ul
							style={{
								display: 'inline-block',
								alignItems: 'center',
								justifyContent: 'center',
							}}
						>
							{Object.values(socials.byId).map((item, index) => (
								<li style={{ float: 'left' }} key={index}>
									{item.link.startsWith('https://') ? (
										<a href={item.link} target="_blank" rel="noreferrer">
											<i className={item.icon}></i>
										</a>
									) : (
										<Link to={item.link} target="_blank" rel="noopener">
											<i className={item.icon}></i>
										</Link>
									)}
								</li>
							))}
						</ul>
					</div>
				</div>
			</footer>
		</div>
	);
};

export default Footer;
