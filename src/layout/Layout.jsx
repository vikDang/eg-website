import adminApi from 'api/adminAPI';
import useMounted from 'hooks/useMounted';
import { useEffect, useState } from 'react';
import { actions as aboutActions } from 'slices/about';
import { actions as bannerActions } from 'slices/banner';
import { actions as characterActions } from 'slices/character';
import { actions as headerActions } from 'slices/header';
import { actions as investorActions } from 'slices/investor';
import { actions as roadmapActions } from 'slices/roadmap';
import { actions as socialActions } from 'slices/social';
import { actions as tokenActions } from 'slices/token';
import { actions as trailerActions } from 'slices/trailer';
import { actions as footerActions } from 'slices/footer';
import { actions as languageActions } from 'slices/language';

import { useDispatch } from 'store';

function Layout({ children }) {
	const mounted = useMounted();
	const dispatch = useDispatch();
	const [isLoading, setIsLoading] = useState(true);

	useEffect(() => {
		async function initAppData() {
			try {
				const response = await adminApi.getAllResource();

				if (mounted.current) {
					const header = response.header;
					const topBanner = response.banner;
					const token = response.token;
					const trailer = response.trailer;
					const abouts = response.listAbout;
					const decorationBanner = response.decorationBanner;
					const character = response.listCharacter;
					const roadmap = response.roadmap;
					const investor = response.investor;
					const social = response.social;
					const languages = response.languages;
					const footer = response.footer;

					if (header) {
						dispatch(headerActions.getMenus(header));
					}
					if (topBanner) {
						dispatch(bannerActions.getTopBanner(topBanner));
					}
					if (token) {
						dispatch(tokenActions.getToken(token));
					}
					if (trailer) {
						dispatch(trailerActions.updateTrailerLink(trailer));
					}
					if (abouts) {
						dispatch(aboutActions.getAbouts(abouts));
					}
					if (decorationBanner) {
						dispatch(bannerActions.getBottomBannerBackGround(decorationBanner));
					}
					if (character) {
						dispatch(characterActions.getCharacters(character.list));
						dispatch(characterActions.updateCharacterTitle(character.title));
					}
					if (roadmap) {
						dispatch(roadmapActions.getRoadmap(roadmap));
					}
					if (investor) {
						dispatch(investorActions.getInvestors(investor.list));
						dispatch(
							investorActions.updateInvestorContent({ title: investor.title, comingSoonText: investor.comingSoonText })
						);
					}
					if (social) {
						dispatch(socialActions.getSocials(social));
					}
					if (languages) {
						dispatch(languageActions.getLanguages(languages));
					}
					if (footer) {
						dispatch(footerActions.getFooter(footer));
					}
				}
				setIsLoading(false);
			} catch (error) {
				console.dir(error);
				setIsLoading(false);
			}
		}
		initAppData();
	}, []);
	return <div>{!isLoading && children}</div>;
}

export default Layout;
