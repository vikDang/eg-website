import adminApi from 'api/adminAPI';
import { useLayoutEffect } from 'react';
import { actions } from 'slices/about';
import { useDispatch } from 'store';
import { toBase64 } from 'utils/toBase64';
import useMounted from './useMounted';

export const useGetAboutImages = about => {
	const mounted = useMounted();
	const dispatch = useDispatch();

	useLayoutEffect(() => {
		const getAboutImage = async about => {
			try {
				const promiseArr = [];
				const isUpdateImage = Boolean(about.description && !about.imageBase64);
				const isUpdateIcon = Boolean(about.icon && !about.imageIconBase64);
				if (isUpdateImage) {
					const imagePromise = adminApi.getAboutImage({ name: about.description });
					promiseArr.push(imagePromise);
				}
				if (isUpdateIcon) {
					const iconPromise = adminApi.getAboutImage({ name: about.icon });
					promiseArr.push(iconPromise);
				}
				const responseArr = await Promise.all(promiseArr);
				if (mounted.current) {
					let imageBase64 = null;
					let imageIconBase64 = null;
					if (responseArr.length === 2) {
						imageBase64 = await toBase64(responseArr[0]);
						imageIconBase64 = await toBase64(responseArr[1]);
					} else if (responseArr.length === 1) {
						imageBase64 = isUpdateImage ? await toBase64(responseArr[0]) : null;
						imageIconBase64 = isUpdateIcon ? await toBase64(responseArr[0]) : null;
					}

					const newAbout = { ...about };
					if (imageBase64) {
						newAbout.imageBase64 = imageBase64;
					}
					if (imageIconBase64) {
						newAbout.imageIconBase64 = imageIconBase64;
					}

					dispatch(actions.updateAbout(newAbout));
				}
			} catch (error) {
				console.log('The following error occured', error);
			}
		};
		getAboutImage(about);
	}, []);
};
