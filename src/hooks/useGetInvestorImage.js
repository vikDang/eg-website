import adminApi from 'api/adminAPI';
import { useEffect } from 'react';
import { actions as investorActions } from 'slices/investor';
import { useDispatch } from 'store';
import { toBase64 } from 'utils/toBase64';
import useMounted from './useMounted';

export const useGetInvestorImage = investor => {
	const mounted = useMounted();
	const dispatch = useDispatch();

	useEffect(() => {
		const getInvestorImage = async imageName => {
			try {
				const response = await adminApi.getInvestorImage({ name: imageName });
				if (mounted.current) {
					const imageBase64 = await toBase64(response);
					const newInvestor = { ...investor };

					dispatch(investorActions.updateInvestor({ ...newInvestor, imageBase64 }));
				}
			} catch (error) {
				console.log('The following error occured', error);
			}
		};
		if (investor?.image && !investor?.imageBase64) {
			getInvestorImage(investor.image);
		}
	}, [investor]);
};
