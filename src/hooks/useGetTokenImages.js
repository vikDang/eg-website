import adminApi from 'api/adminAPI';
import { useEffect } from 'react';
import { actions as tokenActions } from 'slices/token';
import { useDispatch } from 'store';
import { toBase64 } from 'utils/toBase64';
import useMounted from './useMounted';

export const useGetTokenImages = (token, languageCode) => {
	const mounted = useMounted();
	const dispatch = useDispatch();

	useEffect(() => {
		const getTokenImage = async imageName => {
			try {
				const response = await adminApi.getTokenImage({ name: imageName });
				if (mounted.current) {
					const base64Image = await toBase64(response);
					dispatch(tokenActions.updateImageBase64(base64Image));
				}
			} catch (error) {
				console.log('The following error occured', error);
			}
		};
		if (token.tokenImage?.[languageCode]) {
			getTokenImage(token.tokenImage[languageCode]);
		} else {
			dispatch(tokenActions.updateImageBase64(null));
		}
	}, [token.tokenImage?.[languageCode], languageCode]);

	useEffect(() => {
		const getTokenImageMobile = async imageName => {
			try {
				const response = await adminApi.getTokenImage({ name: imageName });
				if (mounted.current) {
					const base64Image = await toBase64(response);
					dispatch(tokenActions.updateImageMobileBase64(base64Image));
				}
			} catch (error) {
				console.log('The following error occured', error);
			}
		};
		if (token.tokenImageMobile?.[languageCode]) {
			getTokenImageMobile(token.tokenImageMobile[languageCode]);
		} else {
			dispatch(tokenActions.updateImageMobileBase64(null));
		}
	}, [token.tokenImageMobile?.[languageCode], languageCode]);
};
