import { useEffect } from 'react';
import { useDispatch } from 'store';
import { toBase64 } from 'utils/toBase64';
import useMounted from './useMounted';
import { actions as roadmapActions } from 'slices/roadmap';
import adminApi from 'api/adminAPI';
import { useSelector } from '../store/index';
import { roadmapSelector } from 'utils/selectors';

export const useGetRoadmapImages = (roadmap, languageCode) => {
	const mounted = useMounted();
	const dispatch = useDispatch();
	const roadmapState = useSelector(roadmapSelector);

	useEffect(() => {
		const getRoadmapContentImage = async imageName => {
			try {
				const response = await adminApi.getRoadmapImage({ name: imageName });
				if (mounted.current) {
					const base64Image = await toBase64(response);
					dispatch(roadmapActions.updateContentImageBase64(base64Image));
				}
			} catch (error) {
				console.log('The following error occured', error);
			}
		};
		if (roadmap.contentImage?.[languageCode]) {
			getRoadmapContentImage(roadmap.contentImage[languageCode]);
		} else {
			dispatch(roadmapActions.updateContentImageBase64(null));
		}
	}, [roadmap.contentImage?.[languageCode], languageCode]);

	useEffect(() => {
		const getRoadmapTopImage = async imageName => {
			try {
				const response = await adminApi.getRoadmapImage({ name: imageName });
				if (mounted.current) {
					const base64Image = await toBase64(response);
					dispatch(roadmapActions.updateTopImageBase64(base64Image));
				}
			} catch (error) {
				console.log('The following error occured', error);
			}
		};
		if (roadmap.topImage && !roadmapState.topImageBase64) {
			getRoadmapTopImage(roadmap.topImage);
		}
	}, [roadmap.topImage]);

	useEffect(() => {
		const getRoadmapBottomImage = async imageName => {
			try {
				const response = await adminApi.getRoadmapImage({ name: imageName });
				if (mounted.current) {
					const base64Image = await toBase64(response);
					dispatch(roadmapActions.updateBottomImageBase64(base64Image));
				}
			} catch (error) {
				console.log('The following error occured', error);
			}
		};
		if (roadmap.bottomImage && !roadmapState.bottomImageBase64) {
			getRoadmapBottomImage(roadmap.bottomImage);
		}
	}, [roadmap.bottomImage]);
};
