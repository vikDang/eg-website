import React, { useEffect } from 'react';

function useShowWhenScroll(id, delay) {
	const showInViewPort = (id, delay = 0) => {
		const element = document.querySelector(id);
		const rect = element.getBoundingClientRect();

		setTimeout(() => {
			if (rect.bottom > 0 && rect.y < window.innerHeight) {
				element.classList.add('show');
			} else {
				element.classList.remove('show');
			}
		}, delay);
	};

	useEffect(() => {
		window.addEventListener('scroll', () => {
			showInViewPort(id, delay);
		});

		return () => {
			window.removeEventListener('scroll', () => {
				showInViewPort(id, delay);
			});
		};
	}, []);
	return;
}

export default useShowWhenScroll;
