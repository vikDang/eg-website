import adminApi from 'api/adminAPI';
import { useLayoutEffect } from 'react';
import { actions } from 'slices/character';
import { useDispatch } from 'store';
import { toBase64 } from 'utils/toBase64';
import useMounted from './useMounted';

export const useGetCharacterImages = characters => {
	const mounted = useMounted();
	const dispatch = useDispatch();

	useLayoutEffect(() => {
		const getCharacterImage = async character => {
			try {
				const promiseArr = [];
				const isUpdateAvatar = Boolean(character.avatar && !character.avatarBase64);
				const isUpdateIcon = Boolean(character.icon && !character.iconBase64);
				const isUpdateIconInactive = Boolean(character.iconInactive && !character.iconInactiveBase64);
				if (isUpdateAvatar) {
					const imagePromise = adminApi.getCharacterImage({ name: character.avatar });
					promiseArr.push(imagePromise);
				} else {
					promiseArr.push(Promise.resolve());
				}
				if (isUpdateIcon) {
					const iconPromise = adminApi.getCharacterImage({ name: character.icon });
					promiseArr.push(iconPromise);
				} else {
					promiseArr.push(Promise.resolve());
				}
				if (isUpdateIconInactive) {
					const iconInactivePromise = adminApi.getCharacterImage({ name: character.iconInactive });
					promiseArr.push(iconInactivePromise);
				} else {
					promiseArr.push(Promise.resolve());
				}
				const responseArr = await Promise.all(promiseArr);
				if (mounted.current) {
					let avatarBase64 = null;
					let iconBase64 = null;
					let iconInactiveBase64 = null;
					if (isUpdateAvatar) {
						avatarBase64 = await toBase64(responseArr[0]);
						dispatch(actions.updateAvatarBase64({ avatarBase64, character }));
					}

					if (isUpdateIcon) {
						iconBase64 = await toBase64(responseArr[1]);
						dispatch(actions.updateIconBase64({ iconBase64, character }));
					}

					if (isUpdateIconInactive) {
						iconInactiveBase64 = await toBase64(responseArr[2]);
						dispatch(actions.updateIconInactiveBase64({ iconInactiveBase64, character }));
					}
				}
			} catch (error) {
				console.log('The following error occured', error);
			}
		};
		characters.forEach(character => getCharacterImage(character));
	}, []);
};
