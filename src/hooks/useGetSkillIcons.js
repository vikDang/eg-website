import adminApi from 'api/adminAPI';
import { useLayoutEffect } from 'react';
import { actions } from 'slices/character';
import { useDispatch } from 'store';
import { toBase64 } from 'utils/toBase64';
import useMounted from './useMounted';

export const useGetSkillIcons = (skill, characterId) => {
	const mounted = useMounted();
	const dispatch = useDispatch();

	useLayoutEffect(() => {
		const getSkillIconImage = async skillIcon => {
			try {
				const response = await adminApi.getCharacterImage({ name: skillIcon });

				if (mounted.current) {
					const iconBase64 = await toBase64(response);

					const newSkill = { ...skill };
					newSkill.iconBase64 = iconBase64;
					dispatch(actions.updateSkill({ skill: newSkill, characterId }));
				}
			} catch (error) {
				console.log('The following error occured', error);
			}
		};
		if (skill?.skillIcon && characterId) {
			getSkillIconImage(skill.skillIcon);
		}
	}, [skill.skillIcon, characterId]);
};
