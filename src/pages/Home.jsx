import React from 'react';
import 'video-react/dist/video-react.css';
import Footer from '../components/footer/Footer';
import Header from '../components/header/Header';
import Banner from '../components/home/banner/Banner';
import CharacterTablet from '../components/home/character/CharacterTablet';
import CharacterMobile from '../components/home/character/CharacterMobile';
import About from '../components/home/feature/About';
import HomeFeature from '../components/home/feature/HomeFeature';
import Investor from '../components/home/investor/Investor';
import Roadmap from '../components/home/roadmap/Roadmap';
import RoadmapMobile from '../components/home/roadmap/RoadmapMobile';
import VideoBg from '../components/home/videoBg/VideoBg';
import useWindowSize from '../hooks/useResize';
import CharacterDesktop from '../components/home/character/CharacterDesktop';

const Home = () => {
	const size = useWindowSize();

	return (
		<div className="home-1">
			<Header />
			<Banner />
			<div className="home__section">
				<div className="home__background"></div>
				<div className="home-feature">
					<HomeFeature />
					<About />
				</div>
			</div>

			{size.width > 1200 && <CharacterDesktop />}
			{size.width <= 1200 && size.width > 991 && <CharacterTablet />}
			{size.width <= 991 && <CharacterMobile />}

			<div className="roadmap-box">
				<div className="roadmap__bg"></div>
				{size.width > 991 ? <Roadmap /> : <RoadmapMobile />}
				<Investor />
			</div>
			<VideoBg />
			<Footer />
		</div>
	);
};

export default Home;
