import Home from './Home';
import Discord from './Discord';
import Telegram from './Telegram';

const routes = [
	{ path: '/', component: <Home /> },
	{ path: '/discord', component: <Discord /> },
	{ path: '/telegram', component: <Telegram /> },
];

export default routes;
