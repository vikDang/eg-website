const menus = [
	{
		id: 1,
		name: 'Home',
		links: '#banner',
	},
	{
		id: 2,
		name: 'Feature',
		links: '#feature',
	},
	{
		id: 3,
		name: 'Token',
		links: '#about-elo',
	},
	{
		id: 4,
		name: 'Paper',
		links: 'https://docs.eternalglory.io/',
	},
];

export default menus;
