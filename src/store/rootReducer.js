import { combineReducers } from '@reduxjs/toolkit';
import { reducer as settingsReducer } from 'slices/settings';
import { reducer as headerReducer } from 'slices/header';
import { reducer as bannerReducer } from 'slices/banner';
import { reducer as tokenReducer } from 'slices/token';
import { reducer as trailerReducer } from 'slices/trailer';
import { reducer as aboutReducer } from 'slices/about';
import { reducer as characterReducer } from 'slices/character';
import { reducer as roadmapReducer } from 'slices/roadmap';
import { reducer as investorReducer } from 'slices/investor';
import { reducer as socialReducer } from 'slices/social';
import { reducer as footerReducer } from 'slices/footer';
import { reducer as languageReducer } from 'slices/language';

const rootReducer = combineReducers({
	settings: settingsReducer,
	header: headerReducer,
	banner: bannerReducer,
	token: tokenReducer,
	trailer: trailerReducer,
	about: aboutReducer,
	character: characterReducer,
	roadmap: roadmapReducer,
	investor: investorReducer,
	social: socialReducer,
	language: languageReducer,
	footer: footerReducer,
});

export default rootReducer;
