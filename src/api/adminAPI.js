import axiosClient from 'lib/axios';
import { configLanguage } from 'utils/config';

const settingPrefix = 'setting';
const aboutPrefix = 'about';
const tokenPrefix = 'token';
const characterPrefix = 'character';
const roadmapPrefix = 'roadmap';
const investorPrefix = 'investor';

const api = {
	//Get all resource
	getAllResource: () => {
		return axiosClient.get(`/${settingPrefix}`);
	},

	/** HEADER */
	// Post Menus
	updateMenus: request => {
		return axiosClient.post(`/${settingPrefix}/header`, request);
	},

	/** BANNER */
	//Get top banner image
	getTopBannerImage: params => {
		return axiosClient.get(`/${settingPrefix}/file/banner`, {
			params,
			responseType: 'blob',
		});
	},

	//Get bottom banner image
	getBottomBannerImage: params => {
		return axiosClient.get(`/${settingPrefix}/file/decoration-banner`, {
			params,
			responseType: 'blob',
		});
	},

	// Update banner content
	updateTopBannerContent: request => {
		let storageLanguage = configLanguage.get() || 'en';

		const newTitle = { [storageLanguage]: request.title };
		const newButtons = request.buttons.map(button => ({ ...button, name: { [storageLanguage]: button.name } }));
		return axiosClient.post(`/${settingPrefix}/banner`, { title: newTitle, buttons: newButtons });
	},

	// Post banner image
	updateTopBannerImage: request => {
		return axiosClient.post(`/${settingPrefix}/banner/background`, request);
	},

	// Post banner mobile image
	updateTopBannerMobileImage: request => {
		return axiosClient.post(`/${settingPrefix}/banner/background-mobile`, request);
	},

	// Post banner image
	updateBottomBannerImage: request => {
		return axiosClient.post(`/${settingPrefix}/decoration-banner`, request);
	},

	/** TRAILER */
	updateTrailer: request => {
		return axiosClient.post(`/${settingPrefix}/trailer`, request);
	},

	/** ABOUT */
	getAllAbout: () => {
		return axiosClient.get(`/${aboutPrefix}`);
	},

	getSingleAbout: id => {
		return axiosClient.get(`/${aboutPrefix}/${id}`);
	},

	getAboutImage: params => {
		return axiosClient.get(`/${settingPrefix}/file/${aboutPrefix}`, {
			params,
			responseType: 'blob',
		});
	},

	createAbout: request => {
		let storageLanguage = configLanguage.get() || 'en';

		return axiosClient.post(`/${aboutPrefix}`, {
			title: { [storageLanguage]: request.title },
			content: { [storageLanguage]: request.content },
		});
	},

	updateAboutContent: (request, id) => {
		let storageLanguage = configLanguage.get() || 'en';

		return axiosClient.patch(`/${aboutPrefix}/${id}`, {
			title: { [storageLanguage]: request.title },
			content: { [storageLanguage]: request.content },
		});
	},

	updateAboutImage: (request, id) => {
		return axiosClient.post(`/${aboutPrefix}/${id}/description`, request);
	},

	updateAboutIcon: (request, id) => {
		return axiosClient.post(`/${aboutPrefix}/${id}/icon`, request);
	},

	deleteAbout: aboutId => {
		return axiosClient.delete(`/${aboutPrefix}/${aboutId}`);
	},

	/** TOKEN */
	getTokenImage: params => {
		return axiosClient.get(`/${settingPrefix}/file/${tokenPrefix}`, {
			params,
			responseType: 'blob',
		});
	},

	updateTokenDescription: request => {
		let storageLanguage = configLanguage.get() || 'en';

		return axiosClient.post(`/${settingPrefix}/${tokenPrefix}`, {
			description: { [storageLanguage]: request.description },
		});
	},

	updateTokenImage: request => {
		return axiosClient.post(`/${settingPrefix}/${tokenPrefix}/image`, request);
	},

	updateTokenImageMobile: request => {
		return axiosClient.post(`/${settingPrefix}/${tokenPrefix}/image-mobile`, request);
	},

	/** CHARACTER */
	getAllCharacter: () => {
		return axiosClient.get(`/${characterPrefix}`);
	},
	getSingleCharacter: characterId => {
		return axiosClient.get(`/${characterPrefix}/${characterId}`);
	},
	getCharacterImage: params => {
		return axiosClient.get(`/${settingPrefix}/file/${characterPrefix}`, {
			params,
			responseType: 'blob',
		});
	},
	createCharacter: request => {
		let storageLanguage = configLanguage.get() || 'en';

		return axiosClient.post(`/${characterPrefix}`, {
			name: { [storageLanguage]: request.name },
			description: { [storageLanguage]: request.description },
		});
	},

	updateCharacterContent: (characterId, request) => {
		let storageLanguage = configLanguage.get() || 'en';

		return axiosClient.patch(`/${characterPrefix}/${characterId}`, {
			name: { [storageLanguage]: request.name },
			description: { [storageLanguage]: request.description },
		});
	},
	updateCharacterPosition: (characterId, request) => {
		return axiosClient.patch(`/${characterPrefix}/position`, request);
	},
	updateCharacterAvatar: (characterId, request) => {
		return axiosClient.post(`/${characterPrefix}/${characterId}/avatar`, request);
	},
	updateCharacterIcon: (characterId, request) => {
		return axiosClient.post(`/${characterPrefix}/${characterId}/icon`, request);
	},
	updateCharacterIconActive: (characterId, request) => {
		return axiosClient.post(`/${characterPrefix}/${characterId}/icon-inactive`, request);
	},

	deleteCharacter: characterId => {
		return axiosClient.delete(`/${characterPrefix}/${characterId}`);
	},

	createSkill: (characterId, request) => {
		let storageLanguage = configLanguage.get() || 'en';

		return axiosClient.post(`/${characterPrefix}/${characterId}/skill`, { info: { [storageLanguage]: request.info } });
	},
	updateSkillInfo: (characterId, skillId, request) => {
		let storageLanguage = configLanguage.get() || 'en';

		return axiosClient.patch(`/${characterPrefix}/${characterId}/skill/${skillId}`, {
			info: { [storageLanguage]: request.info },
		});
	},
	updateSkillIcon: (characterId, skillId, request) => {
		return axiosClient.post(`/${characterPrefix}/${characterId}/skill/icon/${skillId}`, request);
	},

	deleteSkill: (characterId, skillId) => {
		return axiosClient.delete(`/${characterPrefix}/${characterId}/skill/${skillId}`);
	},

	/** ROADMAP */
	getRoadmapImage: params => {
		return axiosClient.get(`/${settingPrefix}/file/${roadmapPrefix}`, {
			params,
			responseType: 'blob',
		});
	},
	updateRoadmapTopImage: request => {
		return axiosClient.post(`/${settingPrefix}/${roadmapPrefix}/top-image`, request);
	},
	updateRoadmapBottomImage: request => {
		return axiosClient.post(`/${settingPrefix}/${roadmapPrefix}/bottom-image`, request);
	},
	updateRoadmapInfo: request => {
		return axiosClient.post(`/${settingPrefix}/${roadmapPrefix}`, request);
	},

	// Investor
	getInvestorImage: params => {
		return axiosClient.get(`/${settingPrefix}/file/${investorPrefix}`, {
			params,
			responseType: 'blob',
		});
	},
	createInvestors: request => {
		return axiosClient.post(`/${settingPrefix}/${investorPrefix}`, request);
	},
	updateInvestorPosition: request => {
		return axiosClient.patch(`/${settingPrefix}/${investorPrefix}/positon`, request);
	},
	deleteInvestor: fileName => {
		return axiosClient.delete(`/${settingPrefix}/${investorPrefix}/${fileName}`);
	},

	// Social
	updateSocials: request => {
		return axiosClient.post(`/${settingPrefix}/social`, request);
	},
};

export default api;
